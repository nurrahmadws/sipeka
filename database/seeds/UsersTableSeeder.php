<?php

use Illuminate\Database\Seeder;
use Ramsey\Uuid\Uuid;
use Illuminate\Support\Str;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new \App\Models\User;
        $user->id = Uuid::uuid4();
        $user->name = "hrd";
        $user->email = "hrd@gmail.com";
        $user->password = \Hash::make("password");
        $user->token = Str::random(32);
        $user->role = "1";
        $user->save();
        $this->command->info("User berhasil diinsert");
    }
}
