<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEducationsTabel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('educations', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('applicant_id');
            $table->string('name')->nullable();
            $table->string('entry_year')->nullable();
            $table->string('graduation_year')->nullable();
            $table->string('paket_c')->nullable();
            $table->string('institution_name')->nullable();
            $table->string('entry_year_paket_c')->nullable();
            $table->string('graduation_year_paket_c')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('educations');
    }
}
