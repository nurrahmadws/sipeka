$(function(){
    $("#jobTable").DataTable();
});

function addCategoryJob()
{
    $("#add_category_job").modal('show');
}

function editCategory(id)
{
    $.ajax({
        type: "GET",
        url: "/hrd/job-category/" + id,
        success: function (data) {
            console.log(data);
            $("#edit_category_job").modal('show');
            $("#frmEditCategoryJob input[name=name]").val(data.detail.name);
            $("#frmEditCategoryJob input[name=id]").val(data.detail.id);
        },
        error: function(data){
            console.log(data);
        }
    });
}

function deleteCategory(id)
{
    $.ajax({
        type: "GET",
        url: "/hrd/job-category/" + id,
        success: function (data) {
            console.log(data);
            $("#delete-category_job").modal('show');
            $("#form_delete_category_job #delete-title").html("Hapus Kateogri (" + data.detail.name + ")?");
            $("#form_delete_category_job input[name=id]").val(data.detail.id);
        },
        error: function(data){
            console.log(data);
        }
    });
}

$(document).ready(function(){
    $('#frmAddCategoryJob').on('submit', function(e){
        e.preventDefault();
        $.ajaxSetup({
            headers:{
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url: "/hrd/job-category",
            data: new FormData(this),
            contentType: false,
            cache:false,
            processData: false,
            dataType: "json",
            beforeSend: function(){
                $('.go').addClass('btn-progress');
            },
            success: function (response) {

                var html = '';
                if(response.errors){
                    html = '<div class="alert alert-danger">';
                    for(var count = 0; count < response.errors.length; count++)
                    {
                        html += '<p>' + response.errors[count] + "</p>";
                    }
                    html += '</div>';
                    $('.go').removeClass('btn-progress');
                }
                if (response.success) {
                    html = '<div class="alert alert-success"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + response.success + '</div>';
                    $('#frmAddCategoryJob')[0].reset();
                    setTimeout(function(){
                        location.reload();
                    }, 1000);
                }
                $('#form_result').html(html);
            }
        });
    });

    $('#frmEditCategoryJob').on('submit', function(e){
        e.preventDefault();
        $.ajaxSetup({
            headers:{
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url: "/hrd/job-categoryy/" + $("#frmEditCategoryJob input[name=id]").val(),
            data: new FormData(this),
            contentType: false,
            cache:false,
            processData: false,
            dataType: "json",
            beforeSend: function(){
                $('.go').addClass('btn-progress');
            },
            success: function (response) {

                var html = '';
                if(response.errors){
                    html = '<div class="alert alert-danger">';
                    for(var count = 0; count < response.errors.length; count++)
                    {
                        html += '<p>' + response.errors[count] + "</p>";
                    }
                    html += '</div>';
                    $('.go').removeClass('btn-progress');
                }
                if (response.success) {
                    html = '<div class="alert alert-success"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + response.success + '</div>';
                    $('#frmEditCategoryJob')[0].reset();
                    setTimeout(function(){
                        location.reload();
                    }, 1000);
                }
                $('#form_result_edit').html(html);
            }
        });
    });

    $("#btn-delete").click(function(){
        $.ajaxSetup({
            headers:{
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'DELETE',
            url: '/hrd/category-job/' + $("#form_delete_category_job input[name=id]").val(),
            dataType: 'json',
            beforeSend: function(){
                $('#btn-delete').addClass('btn-progress');
            },
            success: function(data){
                if (data.success) {
                    html = '<div class="alert alert-success"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + data.success + '</div>';
                    $('#form_delete_category_job')[0].reset();
                    setTimeout(function(){
                        location.reload();
                    }, 1000);
                }
                $('#form_result_delete').html(html);
            },
            error: function(data){
                console.log(data);
            }
        });
    });
});
