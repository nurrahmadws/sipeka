function ShowAddPart()
{
    $("#upload_berkas").modal('show');
}

function deleteBerkas(id)
{
    $.ajax({
        type: "GET",
        url: "/applicant/berkas/" + id,
        success: function (data) {
            console.log(data);
            $("#delete-berkas").modal('show');
            $("#form_delete_berkas #delete-title").html("Hapus Berkas (" + data.detail.title + ")?");
            $("#form_delete_berkas input[name=id]").val(data.detail.id);
        },
        error: function(data){
            console.log(data);
        }
    });
}

$(document).ready(function(){
    $("#frmUploadBerkas").on('submit', function(e){
        e.preventDefault();
        $.ajaxSetup({
            headers:{
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url: "/applicant/berkas",
            data: new FormData(this),
            contentType: false,
            cache:false,
            processData: false,
            dataType: "json",
            beforeSend: function(){
                $('.go').addClass('btn-progress');
            },
            success: function (response) {

                var html = '';
                if(response.errors){
                    html = '<div class="alert alert-danger">';
                    for(var count = 0; count < response.errors.length; count++)
                    {
                        html += '<p>' + response.errors[count] + "</p>";
                    }
                    html += '</div>';
                    $('.go').removeClass('btn-progress');
                }
                if (response.success) {
                    html = '<div class="alert alert-success"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + response.success + '</div>';
                    $('#frmUploadBerkas')[0].reset();
                    setTimeout(function(){
                        location.reload();
                    }, 1000);
                }
                $('#form_result').html(html);
            }
        });
    });

    $("#frmEditBerkas").on('submit', function(e){
        e.preventDefault();
        $.ajaxSetup({
            headers:{
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url: "/applicant/berkass/" + $("#frmEditBerkas input[name=id]").val(),
            data: new FormData(this),
            contentType: false,
            cache:false,
            processData: false,
            dataType: "json",
            beforeSend: function(){
                $('.go').addClass('btn-progress');
            },
            success: function (response) {

                var html = '';
                if(response.errors){
                    html = '<div class="alert alert-danger">';
                    for(var count = 0; count < response.errors.length; count++)
                    {
                        html += '<p>' + response.errors[count] + "</p>";
                    }
                    html += '</div>';
                    $('.go').removeClass('btn-progress');
                }
                if (response.success) {
                    html = '<div class="alert alert-success"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + response.success + '</div>';
                    $('#frmEditBerkas')[0].reset();
                    setTimeout(function(){
                        location.reload();
                    }, 1000);
                }
                $('#form_result_edit').html(html);
            }
        });
    });

    $("#btn-delete").click(function(){
        $.ajaxSetup({
            headers:{
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'DELETE',
            url: '/applicant/berkass/' + $("#form_delete_berkas input[name=id]").val(),
            dataType: 'json',
            beforeSend: function(){
                $('#btn-delete').addClass('btn-progress');
            },
            success: function(data){
                if (data.success) {
                    html = '<div class="alert alert-success"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + data.success + '</div>';
                    $('#form_delete_berkas')[0].reset();
                    setTimeout(function(){
                        location.reload();
                    }, 1000);
                }
                $('#form_result_delete').html(html);
            },
            error: function(data){
                console.log(data);
            }
        });
    });
});
