function showAddPart()
{
    $(document).ready(function(){
        $('.info').hide();
        $('.addPart').slideToggle();
    });
}

function closeAddPart()
{
    $(document).ready(function(){
        $('.info').fadeIn();
        $('.addPart').fadeOut();
    });
}

function editPendidikan(id)
{
    $.ajax({
        type: "GET",
        url: "/applicant/education/" + id,
        success: function (data) {
            console.log(data);
            $("#edit-pendidikan").modal('show');
            $("#frmEditPendidikan input[name=name]").val(data.detail.name);
            $("#frmEditPendidikan input[name=id]").val(data.detail.id);
            $("#frmEditPendidikan input[name=entry_year]").val(data.detail.entry_year);
            $("#frmEditPendidikan input[name=graduation_year]").val(data.detail.graduation_year);
            $("#frmEditPendidikan input[name=type]").val(data.detail.type);
        },
        error: function(data){
            console.log(data);
        }
    });
}

function deletePendidikan(id)
{
    $.ajax({
        type: "GET",
        url: "/applicant/education/" + id,
        success: function (data) {
            console.log(data);
            $("#delete-pendidikan").modal('show');
            $("#form_delete_pendidikan #delete-title").html("Hapus Data (" + data.detail.name + ")?");
            $("#form_delete_pendidikan input[name=id]").val(data.detail.id);
        },
        error: function(data){
            console.log(data);
        }
    });
}

$(document).ready(function(){
    $("#frmSd").on('submit', function(e){
        e.preventDefault();
        $.ajaxSetup({
            headers:{
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url: "/applicant/education",
            data: new FormData(this),
            contentType: false,
            cache:false,
            processData: false,
            dataType: "json",
            beforeSend: function(){
                $('.go').addClass('btn-progress');
            },
            success: function (response) {

                var html = '';
                if(response.errors){
                    html = '<div class="alert alert-danger">';
                    for(var count = 0; count < response.errors.length; count++)
                    {
                        html += '<p>' + response.errors[count] + "</p>";
                    }
                    html += '</div>';
                    $('.go').removeClass('btn-progress');
                }
                if (response.success) {
                    html = '<div class="alert alert-success"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + response.success + '</div>';
                    $('#frmSd')[0].reset();
                    setTimeout(function(){
                        location.reload();
                    }, 1000);
                }
                $('#form_result_sd').html(html);
            }
        });
    });

    $("#frmSmp").on('submit', function(e){
        e.preventDefault();
        $.ajaxSetup({
            headers:{
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url: "/applicant/education",
            data: new FormData(this),
            contentType: false,
            cache:false,
            processData: false,
            dataType: "json",
            beforeSend: function(){
                $('.go').addClass('btn-progress');
            },
            success: function (response) {

                var html = '';
                if(response.errors){
                    html = '<div class="alert alert-danger">';
                    for(var count = 0; count < response.errors.length; count++)
                    {
                        html += '<p>' + response.errors[count] + "</p>";
                    }
                    html += '</div>';
                    $('.go').removeClass('btn-progress');
                }
                if (response.success) {
                    html = '<div class="alert alert-success"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + response.success + '</div>';
                    $('#frmSmp')[0].reset();
                    setTimeout(function(){
                        location.reload();
                    }, 1000);
                }
                $('#form_result_smp').html(html);
            }
        });
    });

    $("#frmSmk").on('submit', function(e){
        e.preventDefault();
        $.ajaxSetup({
            headers:{
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url: "/applicant/education",
            data: new FormData(this),
            contentType: false,
            cache:false,
            processData: false,
            dataType: "json",
            beforeSend: function(){
                $('.go').addClass('btn-progress');
            },
            success: function (response) {

                var html = '';
                if(response.errors){
                    html = '<div class="alert alert-danger">';
                    for(var count = 0; count < response.errors.length; count++)
                    {
                        html += '<p>' + response.errors[count] + "</p>";
                    }
                    html += '</div>';
                    $('.go').removeClass('btn-progress');
                }
                if (response.success) {
                    html = '<div class="alert alert-success"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + response.success + '</div>';
                    $('#frmSmk')[0].reset();
                    setTimeout(function(){
                        location.reload();
                    }, 1000);
                }
                $('#form_result_smk').html(html);
            }
        });
    });

    $("#frmS1").on('submit', function(e){
        e.preventDefault();
        $.ajaxSetup({
            headers:{
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url: "/applicant/education",
            data: new FormData(this),
            contentType: false,
            cache:false,
            processData: false,
            dataType: "json",
            beforeSend: function(){
                $('.go').addClass('btn-progress');
            },
            success: function (response) {

                var html = '';
                if(response.errors){
                    html = '<div class="alert alert-danger">';
                    for(var count = 0; count < response.errors.length; count++)
                    {
                        html += '<p>' + response.errors[count] + "</p>";
                    }
                    html += '</div>';
                    $('.go').removeClass('btn-progress');
                }
                if (response.success) {
                    html = '<div class="alert alert-success"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + response.success + '</div>';
                    $('#frmS1')[0].reset();
                    setTimeout(function(){
                        location.reload();
                    }, 1000);
                }
                $('#form_result_s1').html(html);
            }
        });
    });

    $("#frmS2").on('submit', function(e){
        e.preventDefault();
        $.ajaxSetup({
            headers:{
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url: "/applicant/education",
            data: new FormData(this),
            contentType: false,
            cache:false,
            processData: false,
            dataType: "json",
            beforeSend: function(){
                $('.go').addClass('btn-progress');
            },
            success: function (response) {

                var html = '';
                if(response.errors){
                    html = '<div class="alert alert-danger">';
                    for(var count = 0; count < response.errors.length; count++)
                    {
                        html += '<p>' + response.errors[count] + "</p>";
                    }
                    html += '</div>';
                    $('.go').removeClass('btn-progress');
                }
                if (response.success) {
                    html = '<div class="alert alert-success"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + response.success + '</div>';
                    $('#frmS2')[0].reset();
                    setTimeout(function(){
                        location.reload();
                    }, 1000);
                }
                $('#form_result_s2').html(html);
            }
        });
    });

    $("#frmPaketC").on('submit', function(e){
        e.preventDefault();
        $.ajaxSetup({
            headers:{
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url: "/applicant/education",
            data: new FormData(this),
            contentType: false,
            cache:false,
            processData: false,
            dataType: "json",
            beforeSend: function(){
                $('.go').addClass('btn-progress');
            },
            success: function (response) {

                var html = '';
                if(response.errors){
                    html = '<div class="alert alert-danger">';
                    for(var count = 0; count < response.errors.length; count++)
                    {
                        html += '<p>' + response.errors[count] + "</p>";
                    }
                    html += '</div>';
                    $('.go').removeClass('btn-progress');
                }
                if (response.success) {
                    html = '<div class="alert alert-success"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + response.success + '</div>';
                    $('#frmPaketC')[0].reset();
                    setTimeout(function(){
                        location.reload();
                    }, 1000);
                }
                $('#form_result_paketc').html(html);
            }
        });
    });

    $("#frmEditPendidikan").on('submit', function(e){
        e.preventDefault();
        $.ajaxSetup({
            headers:{
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url: "/applicant/educationn/" + $("#frmEditPendidikan input[name=id]").val(),
            data: new FormData(this),
            contentType: false,
            cache:false,
            processData: false,
            dataType: "json",
            beforeSend: function(){
                $('.go').addClass('btn-progress');
            },
            success: function (response) {

                var html = '';
                if(response.errors){
                    html = '<div class="alert alert-danger">';
                    for(var count = 0; count < response.errors.length; count++)
                    {
                        html += '<p>' + response.errors[count] + "</p>";
                    }
                    html += '</div>';
                    $('.go').removeClass('btn-progress');
                }
                if (response.success) {
                    html = '<div class="alert alert-success"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + response.success + '</div>';
                    $('#frmEditPendidikan')[0].reset();
                    setTimeout(function(){
                        location.reload();
                    }, 1000);
                }
                $('#form_result_edit').html(html);
            }
        });
    });

    $("#btn-delete").click(function(){
        $.ajaxSetup({
            headers:{
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'DELETE',
            url: '/applicant/educationn/' + $("#form_delete_pendidikan input[name=id]").val(),
            dataType: 'json',
            beforeSend: function(){
                $('#btn-delete').addClass('btn-progress');
            },
            success: function(data){
                if (data.success) {
                    html = '<div class="alert alert-success"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + data.success + '</div>';
                    $('#form_delete_pendidikan')[0].reset();
                    setTimeout(function(){
                        location.reload();
                    }, 1000);
                }
                $('#form_result_delete').html(html);
            },
            error: function(data){
                console.log(data);
            }
        });
    });
});
