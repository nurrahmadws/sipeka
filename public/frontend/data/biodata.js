function CreateBiodata()
{
    $(document).ready(function(){
        $('.msg').hide();
        $('.bio').slideToggle();
    });
}

function closeCreateData()
{
    $(document).ready(function(){
        $('.msg').fadeIn();
        $('.bio').fadeOut();
    });
}

function closeEditData()
{
    $(document).ready(function(){
        $('.myBio').fadeIn();
        $('.edtbio').fadeOut();
    });
}

function editBiodata(id)
{
    $.ajax({
        type: "GET",
        url: "/applicant/biodata/" + id,
        success:function(data){
            console.log(data);
            $('.myBio').hide();
            $('.edtbio').slideToggle();
            $("#frmEdtBiodata input[name=name]").val(data.detail.name);
            $("#frmEdtBiodata input[name=date_place]").val(data.detail.date_place);
            $("#frmEdtBiodata input[name=date_birth]").val(data.detail.date_birth);
            $("#frmEdtBiodata textarea[name=address_based_id_card]").val(data.detail.address_based_id_card);
            $("#frmEdtBiodata textarea[name=current_address]").val(data.detail.current_address);
            $("#frmEdtBiodata input[name=phone]").val(data.detail.phone);
            $("#frmEdtBiodata input[name=id_identity]").val(data.detail.id_identity);
            $("#frmEdtBiodata input[name=id]").val(data.detail.id);
        },
        error: function(data){
            console.log(data);
        }
    });
}

$(document).ready(function(){
    $("#frmAddBiodata").on('submit', function(e){
        e.preventDefault();
        $.ajaxSetup({
            headers:{
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url: "/applicant/biodata",
            data: new FormData(this),
            contentType: false,
            cache:false,
            processData: false,
            dataType: "json",
            beforeSend: function(){
                $('.go').addClass('btn-progress');
            },
            success: function (response) {

                var html = '';
                if(response.errors){
                    html = '<div class="alert alert-danger">';
                    for(var count = 0; count < response.errors.length; count++)
                    {
                        html += '<p>' + response.errors[count] + "</p>";
                    }
                    html += '</div>';
                    $('.go').removeClass('btn-progress');
                }
                if (response.success) {
                    html = '<div class="alert alert-success"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + response.success + '</div>';
                    $('#frmAddBiodata')[0].reset();
                    setTimeout(function(){
                        location.reload();
                    }, 1000);
                }
                $('#form_result').html(html);
            }
        });
    });
    $("#frmEdtBiodata").on('submit', function(e){
        e.preventDefault();
        $.ajaxSetup({
            headers:{
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url: "/applicant/biodataa/" + $("#frmEdtBiodata input[name=id]").val(),
            data: new FormData(this),
            contentType: false,
            cache:false,
            processData: false,
            dataType: "json",
            beforeSend: function(){
                $('.go').addClass('btn-progress');
            },
            success: function (response) {

                var html = '';
                if(response.errors){
                    html = '<div class="alert alert-danger">';
                    for(var count = 0; count < response.errors.length; count++)
                    {
                        html += '<p>' + response.errors[count] + "</p>";
                    }
                    html += '</div>';
                    $('.go').removeClass('btn-progress');
                }
                if (response.success) {
                    html = '<div class="alert alert-success"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + response.success + '</div>';
                    $('#frmEdtBiodata')[0].reset();
                    setTimeout(function(){
                        location.reload();
                    }, 1000);
                }
                $('#form_result_edit').html(html);
            }
        });
    });
});
