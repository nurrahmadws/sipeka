<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\Models\User;
use App\Models\Category;
use Ramsey\Uuid\Uuid;

class JobCategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('hrdShield');
    }

    public function index()
    {
        $data['categories'] = Category::orderBy('name', 'asc')->get();
        return view('hrd.category.index', $data);
    }

    public function store(Request $request)
    {
        $rules = array(
            'name' => 'required',
        );

        $error = Validator::make($request->all(), $rules);
		if ($error->fails()) {
			return response()->json(['errors'=>$error->errors()->all()]);
        }

        $category = new Category;
        $category->id = Uuid::uuid4();
        $category->name = $request->name;

        $category->save();

        return response()->json([
            'success' => 'Kategori Pekerjaan Berhasil Dibuat'
        ]);
    }

    public function show($id)
    {
        $categories = Category::where('id', $id)->first();
        return response()->json([
            'error' => false,
            'detail'=> $categories
        ]);
    }

    public function update(Request $request, $id)
    {
        $rules = array(
            'name' => 'required',
        );

        $error = Validator::make($request->all(), $rules);
		if ($error->fails()) {
			return response()->json(['errors'=>$error->errors()->all()]);
        }

        $category = Category::where('id', $id)->first();
        $category->name = $request->name;

        $category->save();

        return response()->json([
            'success' => 'Kategori Pekerjaan Berhasil Diperbaharui'
        ]);
    }

    public function destroy($id)
    {
        $category = Category::where('id', $id)->first();
        $category->delete();
        return response()->json([
            'success' => 'Kategori Pekerjaan Berhasil Dihapus'
        ]);
    }
}
