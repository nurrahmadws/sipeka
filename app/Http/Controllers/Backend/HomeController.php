<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Education;
use App\Models\User;
use App\Models\File;
use App\Models\Proposed;
use App\Models\Category;
use App\Models\Applicant;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('hrdShield');
    }

    public function index()
    {
        $data['proposeds'] = Proposed::all();
        // $data['proposed1'] = Proposed::select()
        return view('hrd.home', $data);
    }
}
