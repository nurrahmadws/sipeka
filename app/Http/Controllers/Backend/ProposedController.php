<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Education;
use App\Models\User;
use App\Models\File;
use App\Models\Proposed;
use App\Models\Category;
use App\Models\Applicant;
use Validator;

class ProposedController extends Controller
{
    public function __construct()
    {
        $this->middleware('hrdShield');
    }

    public function index()
    {
        $data['proposeds'] = Proposed::orderBy('created_at', 'asc')->get();
        return view('hrd.proposed.index', $data);
    }

    public function detail($id)
    {
        $data['detail'] = Proposed::where('id', $id)->first();
        $pelamar = $data['detail'];
        $data['applicant'] = Applicant::where('id', $pelamar->applicant_id)->first();
        $data['education'] = Education::where('applicant_id', $pelamar->applicant_id)->get();
        $data['sd'] = Education::where('type', 'sd')->where('applicant_id', $pelamar->applicant_id)->first();
        $data['smp'] = Education::where('type', 'smp')->where('applicant_id', $pelamar->applicant_id)->first();
        $data['smksma'] = Education::where('type', 'smk/sma')->where('applicant_id', $pelamar->applicant_id)->first();
        $data['strata_1'] = Education::where('type', 'strata_1')->where('applicant_id', $pelamar->applicant_id)->first();
        $data['pasca_sarjana'] = Education::where('type', 'pasca_sarjana')->where('applicant_id', $pelamar->applicant_id)->first();
        $data['paket_c'] = Education::where('type', 'paket_c')->where('applicant_id', $pelamar->applicant_id)->first();
        return view('hrd.proposed.detail', $data);
    }

    public function update(Request $request, $id)
    {
        $rules = array(
            'status' => 'required',
            'description' => 'required',
        );

        $error = Validator::make($request->all(), $rules);
		if ($error->fails()) {
			return response()->json(['errors'=>$error->errors()->all()]);
        }

        $proposed = Proposed::where('id', $id)->first();
        $proposed->status = $request->status;
        $proposed->description = $request->description;

        $proposed->update();

        return response()->json([
            'success' => 'Status Berhasil Diperbaharui'
        ]);
    }
}
