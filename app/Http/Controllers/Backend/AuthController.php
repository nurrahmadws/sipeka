<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Ramsey\Uuid\Uuid;
use Validator;

class AuthController extends Controller
{
    public function formLogin()
    {
        return view('hrd.login');
    }

    public function login(Request $request)
    {
        $this->validate($request, [
    		'email' => 'required',
    		'password' => 'required'
        ]);

        $user = User::where('email', $request->email)->first();
        if ($user == NULL) {
            return redirect('hrd/login')->with('message', 'Akun Anda Belum Terdaftar');
        }else{
            if (Hash::check($request->password, $user->password)) {
                $userData = User::where('email', $request->email)->first();
                if ($userData->token == NULL) {
                    $userData->token = Str::random(32);
                    $userData->update();
                }
                if ($user->role == '1') {
                    $result = $user;
                    session(['user_id' => $result->id]);
                    session(['user_back' => $result->name]);
                    session(['email_back' => $result->email]);
                    session(['token_back' => $result->token]);
                    session(['role_back' => $result->role]);
                    return redirect('hrd/home')->with('message', 'Berhasil Login, Selamat Datang '.$user->name);
                }else{
                    return redirect('hrd/login')->with('message', 'Anda Tidak Memiliki Hak Akses Tersebut');
                }
            }else{
                return redirect('hrd/login')->with('message', 'Password Anda Salah');
            }
        }
    }

    public function logout()
    {
        session()->forget(['user_id', 'user_back', 'email_back', 'token_back', 'role_back']);
    	return redirect('/')->with('message', 'Berhasil Logout');
    }

    public function editMyProfile(Request $request, $id)
    {
        $rules = array(
            'name'	=> 'required',
            'email' => 'required|email',
        );

        $error = Validator::make($request->all(), $rules);
		if ($error->fails()) {
			return response()->json(['errors'=>$error->errors()->all()]);
        }

        $user = User::findOrFail($id);
        $user->name  = $request->name;
        $user->email = $request->email;

        $user->save();

        return response()->json([
        	'success' => 'Profile Berhasil Diperbaharui'
        ], 200);
    }

    public function editMyPassword(Request $request, $id)
	{
		$this->validate($request, [
			'old_password' => 'required',
            'password'     => 'required',
        ]);

		$user = User::findOrFail($id);
		if (Hash::check($request->old_password, $user->password)) {
			$user->password = Hash::make($request->password);
			$user->update();
			session()->forget(['user_id', 'user_back', 'email_back', 'token_back', 'role_back']);
			return response()->json([
				'success' => 'Password berhasil diubah, Silahkan Login Kembali Menggunakan password Baru'
			], 200);
		}else{
			return response()->json([
				'error'  => true,
				'message'=> 'Gagal memperbaharui password'
			], 401);
		}
	}

    public function show($id)
	{
		$user = User::findOrFail($id);
		return response()->json([
			'error' => false,
			'detail'	=> $user,
		], 200);
	}
}
