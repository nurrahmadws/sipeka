<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Applicant;
use App\Models\Education;
use App\Models\User;
use Ramsey\Uuid\Uuid;
use Image;
use Validator;
use Illuminate\Support\Str;
use Storage;

class BiodataController extends Controller
{
    public function __construct()
    {
        $this->middleware('applicantShield');
    }

    public function index(Request $request)
    {
        $token = $request->session()->get('token_front');
        $user = User::where('token', $token)->first();
        $data['biodata'] = Applicant::where('user_id', $user->id)->first();
        $data['user'] = $user;
        return view('applicants.biodata.index', $data);
    }

    public function store(Request $request)
    {
        $rules = array(
            'name' => 'required',
            'date_place' => 'required',
            'date_birth' => 'required',
            'gender' => 'required',
            'religion' => 'required',
            'last_education' => 'required',
            'address_based_id_card' => 'required',
            'current_address' => 'required',
            'phone' => 'required|numeric',
            'identity' => 'required',
            'id_identity' => 'required',
            'marital_status' => 'required',
            'photo' => 'required|image',
        );
        $error = Validator::make($request->all(), $rules);
		if ($error->fails()) {
			return response()->json(['errors'=>$error->errors()->all()]);
        }

        $token = $request->session()->get('token_front');
        $user = User::where('token', $token)->first();

        $applicant = new Applicant;
        $applicant->id = Uuid::uuid4();
        $applicant->user_id = $user->id;
        if ($request->name) {
            $applicant->name = $request->name;
            $user->update(['name'=>$request->name]);
        }
        $applicant->date_place = $request->date_place;
        $applicant->date_birth = $request->date_birth;
        $applicant->gender = $request->gender;
        $applicant->religion = $request->religion;
        $applicant->last_education = $request->last_education;
        $applicant->address_based_id_card = $request->address_based_id_card;
        $applicant->current_address = $request->current_address;
        $applicant->phone = $request->phone;
        $applicant->identity = $request->identity;
        $applicant->id_identity = $request->id_identity;
        $applicant->marital_status = $request->marital_status;

        if($request->hasFile('photo'))
        {
            $name = Str::slug($request->name, '_');
            $photo = $request->file('photo');
            $filename = $name . '.' . $photo->getClientOriginalExtension();
            $location = public_path('image/'.$filename);
            Image::make($photo)->resize(200,200)->save($location);
            $applicant->photo = $filename;
        }

        $applicant->save();
        return response()->json([
            'success' => 'Biodata berhasil dibuat'
        ], 200);
    }

    public function show($id)
    {
        $applicant = Applicant::where('id', $id)->first();
        return response()->json([
            'error' => false,
            'detail' => $applicant,
        ], 200);
    }

    public function update(Request $request, $id)
    {
        $rules = array(
            'name' => 'required',
            'date_place' => 'required',
            'date_birth' => 'required',
            'gender' => 'required',
            'religion' => 'required',
            'last_education' => 'required',
            'address_based_id_card' => 'required',
            'current_address' => 'required',
            'phone' => 'required|numeric',
            'identity' => 'required',
            'id_identity' => 'required',
            'marital_status' => 'required',
        );
        $error = Validator::make($request->all(), $rules);
		if ($error->fails()) {
			return response()->json(['errors'=>$error->errors()->all()]);
        }

        $token = $request->session()->get('token_front');
        $user = User::where('token', $token)->first();

        $applicant = Applicant::where('id', $id)->first();
        $applicant->user_id = $user->id;
        if ($request->name) {
            $applicant->name = $request->name;
            $user->update(['name'=>$request->name]);
        }
        $applicant->date_place = $request->date_place;
        $applicant->date_birth = $request->date_birth;
        $applicant->gender = $request->gender;
        $applicant->religion = $request->religion;
        $applicant->last_education = $request->last_education;
        $applicant->address_based_id_card = $request->address_based_id_card;
        $applicant->current_address = $request->current_address;
        $applicant->phone = $request->phone;
        $applicant->identity = $request->identity;
        $applicant->id_identity = $request->id_identity;
        $applicant->marital_status = $request->marital_status;

        if($request->hasFile('photo'))
        {
            $name = Str::slug($request->name, '_');
            $photo = $request->file('photo');
            $filename = $name . '.' . $photo->getClientOriginalExtension();
            $location = public_path('image/'.$filename);
            Image::make($photo)->resize(200,200)->save($location);
            $oldPhoto = $applicant->image;
            $applicant->photo = $filename;
            Storage::delete($oldPhoto);
        }

        $applicant->save();
        return response()->json([
            'success' => 'Biodata berhasil diperbaharui'
        ], 200);
    }
}
