<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Applicant;
use App\Models\File;
use App\Models\User;
use Ramsey\Uuid\Uuid;
use Image;
use Validator;
use Illuminate\Support\Str;

class BerkasController extends Controller
{
    public function __construct()
    {
        $this->middleware('applicantShield');
    }

    public function index(Request $request)
    {
        $token = $request->session()->get('token_front');
        $user = User::where('token', $token)->first();
        $applicant = Applicant::where('user_id', $user->id)->first();
        if ($applicant == NULL) {
            return \Redirect::back()->withErrors(['Lengkapi Biodata Dahulu']);
        }else{
            $data['berkas'] = File::where('applicant_id', $applicant->id)->get();
            $data['cv'] = File::where('title', 'Curriculum Vitae (CV)')->where('applicant_id', $applicant->id)->first();
            $data['portfolio'] = File::where('title', 'Portfolio')->where('applicant_id', $applicant->id)->first();
            $data['ktp'] = File::where('title', 'KTP')->where('applicant_id', $applicant->id)->first();
            $data['sima'] = File::where('title', 'SIM A')->where('applicant_id', $applicant->id)->first();
            $data['simc'] = File::where('title', 'SIM C')->where('applicant_id', $applicant->id)->first();
            $data['ijazah'] = File::where('title', 'Ijazah Terakhir')->where('applicant_id', $applicant->id)->first();
            $data['sertifikat'] = File::where('title', 'Sertifikat/Penghargaan')->where('applicant_id', $applicant->id)->first();
            return view('applicants.berkas.index', $data);
        }
    }

    public function store(Request $request)
    {
        $rules = array(
            'title'       => 'required',
            'file'        => 'required|image',
            'description' => 'required'
        );
        $error = Validator::make($request->all(), $rules);
		if ($error->fails()) {
			return response()->json(['errors'=>$error->errors()->all()]);
        }

        $token = $request->session()->get('token_front');
        $user = User::where('token', $token)->first();
        $applicant = Applicant::where('user_id', $user->id)->first();
        $file = File::where('applicant_id', $applicant->id)->first();

        $berkas = new File;
        $berkas->id = Uuid::uuid4();
        $berkas->applicant_id = $applicant->id;
        $berkas->title = $request->title;
        $berkas->description = $request->description;

        if($request->hasFile('file'))
        {
            $name = Str::slug($request->title, '_');
            $c = Str::slug($applicant->name, '_');
            $file = $request->file('file');
            $filename = $name. '_' . $c . '.' . $file->getClientOriginalExtension();
            $location = public_path('berkas/'.$filename);
            Image::make($file)->resize(750,500)->save($location);
            $berkas->file = $filename;
        }

        $berkas->save();
        return response()->json([
            'success' => 'berkas berhasil dibuat'
        ], 200);
    }

    public function show($id)
    {
        $berkas = File::where('id', $id)->first();
        return response()->json([
            'error' => false,
            'detail'=> $berkas
        ], 200);
    }

    public function deleteImage($file)
    {
        if (file_exists(public_path('/berkas/'.$file))) {
            unlink(public_path('/berkas/'.$file));
        }
    }

    public function update(Request $request, $id)
    {
        $rules = array(
            'title'       => 'required',
            'description' => 'required',
            'file'        => 'image'
        );
        $error = Validator::make($request->all(), $rules);
		if ($error->fails()) {
			return response()->json(['errors'=>$error->errors()->all()]);
        }

        $token = $request->session()->get('token_front');
        $user = User::where('token', $token)->first();
        $applicant = Applicant::where('user_id', $user->id)->first();

        $berkas = File::where('id', $id)->first();
        $berkas->title = $request->title;
        $berkas->description = $request->description;

        if($request->hasFile('file'))
        {
            $name = Str::slug($request->title, '_');
            $c = Str::slug($applicant->name, '_');
            $file = $request->file('file');
            $filename = $name. '_' . $c . '.' . $file->getClientOriginalExtension();
            $location = public_path('berkas/'.$filename);
            Image::make($file)->resize(750,500)->save($location);
            $oldPhoto = $berkas->file;
            $berkas->file = $filename;
            // $this->deleteImage($oldPhoto);
        }

        $berkas->update();
        return response()->json([
            'success' => 'Berkas berhasil diperbaharui'
        ], 200);
    }

    public function destroy($id)
    {
        $berkas = File::where('id', $id)->first();
        $this->deleteImage($berkas->file);
        $berkas->delete();

        return response()->json([
            'success' => 'Berkas Berhasil Dihapus',
        ], 200);
    }
}
