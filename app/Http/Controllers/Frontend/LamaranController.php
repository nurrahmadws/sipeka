<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Applicant;
use App\Models\Education;
use App\Models\User;
use App\Models\File;
use App\Models\Proposed;
use App\Models\Category;
use Ramsey\Uuid\Uuid;
use Image;
use Validator;
use Illuminate\Support\Str;
use Storage;

class LamaranController extends Controller
{
    public function __construct()
    {
        $this->middleware('applicantShield');
    }

    public function index(Request $request)
    {
        $token = $request->session()->get('token_front');
        $user = User::where('token', $token)->first();
        $applicant = Applicant::where('user_id', $user->id)->first();
        if ($applicant == NULL) {
            return \Redirect::back()->withErrors(['Lengkapi Biodata Dahulu']);
        }else{
            $data['categories'] = Category::all();
            $data['berkas'] = File::where('applicant_id', $applicant->id)->get();
            $data['proposeds'] = Proposed::where('applicant_id', $applicant->id)->get();
            return view('applicants.lamaran.index', $data);
        }
    }

    public function store(Request $request)
    {
        $rules = array(
            'application_letter'  => 'required',
            'category_id' => 'required',
        );

        $error = Validator::make($request->all(), $rules);
		if ($error->fails()) {
			return response()->json(['errors'=>$error->errors()->all()]);
        }

        $token = $request->session()->get('token_front');
        $user = User::where('token', $token)->first();
        $applicant = Applicant::where('user_id', $user->id)->first();

        $proposed = new Proposed;
        $proposed->id = Uuid::uuid4();
        $proposed->applicant_id = $applicant->id;
        $proposed->application_letter = $request->application_letter;
        $proposed->category_id = $request->category_id;
        $proposed->status = 'Menunggu Konfirmasi';
        $proposed->description = 'Lamaran Anda berhasil dikirim dan akan segera diproses oleh tim perusahaan';

        $proposed->save();
        $proposed->berkas()->sync($request->berkas, false);

        return response()->json([
			'success' => 'Lamaran Berhasil Dikirim'
		], 200);
    }

    public function detail($id)
    {
        $data['detail'] = Proposed::where('id', $id)->first();
        return view('applicants.lamaran.detail', $data);
    }

    public function edit(Request $request, $id)
    {
        $token = $request->session()->get('token_front');
        $user = User::where('token', $token)->first();
        $applicant = Applicant::where('user_id', $user->id)->first();

        $data['berkas'] = File::where('applicant_id', $applicant->id)->get();
        $data['categories'] = Category::all();
        $data['edit'] = Proposed::where('id', $id)->first();
        return view('applicants.lamaran.edit', $data);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'application_letter'  => 'required',
            'category_id' => 'required',
		]);

        $proposed = Proposed::where('id', $id)->first();
        $proposed->application_letter = $request->application_letter;
        $proposed->category_id = $request->category_id;
        $proposed->status = 'Menunggu Konfirmasi';
        $proposed->description = 'Lamaran Anda berhasil dikirim dan akan segera diproses oleh tim perusahaan';

        $proposed->save();
        if (isset($request->berkas)) {
			$proposed->berkas()->sync($request->berkas);
		}else{
			$proposed->berkas()->sync(array());
        }

        return redirect('applicant/lamaran')->with('message', 'Lamaran Berhasil Diperbaharui');
    }
}
