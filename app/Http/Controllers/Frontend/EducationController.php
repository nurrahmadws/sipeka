<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Applicant;
use App\Models\Education;
use App\Models\User;
use Ramsey\Uuid\Uuid;
use Image;
use Validator;
use Illuminate\Support\Str;
use Storage;

class EducationController extends Controller
{
    public function __construct()
    {
        $this->middleware('applicantShield');
    }

    public function index(Request $request)
    {
        $token = $request->session()->get('token_front');
        $user = User::where('token', $token)->first();
        $applicant = Applicant::where('user_id', $user->id)->first();
        if ($applicant==null) {
            return \Redirect::back()->withErrors(['Lengkapi Biodata Dahulu']);
        }else{
            // $data['educations'] = Education::where('applicant_id', $applicant->id)->get();

        $data['sd'] = Education::where('type', 'sd')->where('applicant_id', $applicant->id)->first();
        $data['smp'] = Education::where('type', 'smp')->where('applicant_id', $applicant->id)->first();
        $data['smksma'] = Education::where('type', 'smk/sma')->where('applicant_id', $applicant->id)->first();
        $data['strata_1'] = Education::where('type', 'strata_1')->where('applicant_id', $applicant->id)->first();
        $data['pasca_sarjana'] = Education::where('type', 'pasca_sarjana')->where('applicant_id', $applicant->id)->first();
        $data['paket_c'] = Education::where('type', 'paket_c')->where('applicant_id', $applicant->id)->first();
        // return $data['sd'];
        return view('applicants.education.index', $data);
        }
    }

    public function store(Request $request)
    {
        $rules = array(
            'name'  => 'required',
            'entry_year' => 'numeric',
            'graduation_year' => 'numeric',
        );
        $error = Validator::make($request->all(), $rules);
		if ($error->fails()) {
			return response()->json(['errors'=>$error->errors()->all()]);
        }

        $token = $request->session()->get('token_front');
        $user = User::where('token', $token)->first();
        $applicant = Applicant::where('user_id', $user->id)->first();

        $educationSd = new Education;
        $educationSd->id = Uuid::uuid4();
        $educationSd->applicant_id = $applicant->id;
        $educationSd->type = $request->type;
        $educationSd->name = $request->name;
        $educationSd->entry_year = $request->entry_year;
        $educationSd->graduation_year = $request->graduation_year;

        $educationSd->save();
        return response()->json([
            'success' => 'Data pendidikan Berhasil Di tambahkan'
        ], 200);
    }

    public function show($id)
    {
        $education = Education::where('id', $id)->first();
        return response()->json([
            'error' => false,
            'detail' => $education
        ]);
    }

    public function update(Request $request, $id)
    {
        $rules = array(
            'name'  => 'required',
            'entry_year' => 'numeric',
            'graduation_year' => 'numeric',
        );
        $error = Validator::make($request->all(), $rules);
		if ($error->fails()) {
			return response()->json(['errors'=>$error->errors()->all()]);
        }

        $education = Education::where('id', $id)->first();
        $education->type = $request->type;
        $education->name = $request->name;
        $education->entry_year = $request->entry_year;
        $education->graduation_year = $request->graduation_year;

        $education->save();
        return response()->json([
            'success' => 'Data pendidikan Berhasil Diperbaharui'
        ], 200);
    }

    public function destroy($id)
    {
        $education = Education::where('id', $id)->first();
        $education->delete();

        return response()->json([
			'success' => 'Data Pendidikan Berhasil Dihapus'
		], 200);
    }
}
