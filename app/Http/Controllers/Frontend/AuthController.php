<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Applicant;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Ramsey\Uuid\Uuid;
use Validator;

class AuthController extends Controller
{
    public function formLogin()
    {
        return view('applicants.login');
    }

    public function formRegister()
    {
        return view('applicants.register');
    }

    public function register(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'confirm_password' => 'required|same:password'
        ]);

        $user = new User;
        $user->id = Uuid::uuid4();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->token = Str::random(32);
        $user->role = '2';

        $result = $user;
        session(['userId' => $result->id]);
        session(['user_front' => $result->name]);
        session(['email_front' => $result->email]);
        session(['token_front' => $result->token]);
        session(['role_front' => $result->role]);

        $user->save();
        return redirect('applicant/home')->with('message', 'Berhasil Registrasi, Selamat Datang '.$user->name);
    }

    public function login(Request $request)
    {
        $this->validate($request, [
    		'email' => 'required',
    		'password' => 'required'
        ]);

        $user = User::where('email', $request->email)->first();
        if ($user == NULL) {
            return redirect('applicant/login')->with('message', 'Akun Anda Belum Terdaftar');
        }else{
            if (Hash::check($request->password, $user->password)) {
                $userData = User::where('email', $request->email)->first();
                if ($userData->token == NULL) {
                    $userData->token = Str::random(32);
                    $userData->update();
                }
                if ($user->role == '2') {
                    $result = $user;
                    session(['userId' => $result->id]);
                    session(['user_front' => $result->name]);
                    session(['email_front' => $result->email]);
                    session(['token_front' => $result->token]);
                    session(['role_front' => $result->role]);
                    return redirect('applicant/home')->with('message', 'Berhasil Login, Selamat Datang '.$user->name);
                }else{
                    return redirect('applicant/login')->with('message', 'Anda Tidak Memiliki Hak Akses Tersebut');
                }
            }else{
                return redirect('applicant/login')->with('message', 'Password Anda Salah');
            }
        }
    }

    public function logout()
    {
        session()->forget(['userId', 'user_front', 'email_front', 'token_front', 'role_front']);
    	return redirect('/')->with('message', 'Berhasil Logout');
    }

    public function editMyProfile(Request $request, $id)
    {
        $rules = array(
            'name'	=> 'required',
            'email' => 'required|email',
        );

        $error = Validator::make($request->all(), $rules);
		if ($error->fails()) {
			return response()->json(['errors'=>$error->errors()->all()]);
        }
        $token = $request->session()->get('token_front');
        $userr = User::where('token', $token)->first();
        $applicant = Applicant::where('user_id', $userr->id)->first();

        $user = User::findOrFail($id);

        if ($request->name) {
            $user->name = $request->name;
            $applicant->update(['name'=>$request->name]);
        }
        $user->name  = $request->name;
        $user->email = $request->email;

        $user->save();

        return response()->json([
        	'success' => 'Profile Berhasil Diperbaharui'
        ], 200);
    }

    public function editMyPassword(Request $request, $id)
	{
		$this->validate($request, [
			'old_password' => 'required',
            'password'     => 'required',
        ]);

		$user = User::findOrFail($id);
		if (Hash::check($request->old_password, $user->password)) {
			$user->password = Hash::make($request->password);
			$user->update();
			session()->forget(['userId', 'user_front', 'email_front', 'token_front', 'role_front']);
			return response()->json([
				'success' => 'Password berhasil diubah, Silahkan Login Kembali Menggunakan password Baru'
			], 200);
		}else{
			return response()->json([
				'error'  => true,
				'message'=> 'Gagal memperbaharui password'
			], 401);
		}
	}

    public function show($id)
	{
		$user = User::findOrFail($id);
		return response()->json([
			'error' => false,
			'detail'	=> $user,
		], 200);
	}
}
