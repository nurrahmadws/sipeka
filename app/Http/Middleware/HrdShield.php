<?php

namespace App\Http\Middleware;

use Closure;

class HrdShield
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (session('user_id')==null) {
            return redirect('hrd/login')->with('message', 'Anda Harus Login Dulu');
        }
        return $next($request);
    }
}
