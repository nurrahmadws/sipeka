<?php

namespace App\Http\Middleware;

use Closure;

class ApplicantShield
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (session('userId')==null) {
            return redirect('applicant/login')->with('message', 'Anda Harus Login Dulu');
        }
        return $next($request);
    }
}
