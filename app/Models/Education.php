<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Education extends Model
{
    protected $table = 'educations';
    protected $fillable = ['id', 'applicant_id', 'type', 'name', 'entry_year', 'graduation_year'];
    public $incrementing = false;

    public function applicant()
    {
        return $this->belongsTo('App\Models\Applicant');
    }
}
