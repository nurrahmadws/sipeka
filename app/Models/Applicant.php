<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Applicant extends Model
{
    protected $table = 'applicants';
    protected $fillable = ['id', 'user_id', 'name', 'date_place', 'date_birth', 'gender', 'religion', 'last_education',
                        'address_based_id_card', 'current_address', 'phone', 'identity', 'id_identity', 'marital_status',
                        'photo'];
    public $incrementing = false;

    public function education()
    {
        return $this->belongsTo('App\Models\Education');
    }

    public function users()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }
}
