<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    protected $table = 'files';
    protected $fillable = ['id', 'applicant_id', 'title', 'file', 'description'];
    public $incrementing = false;

    public function proposed()
    {
        return $this->belongsToMany('App\Models\Proposed');
    }
}
