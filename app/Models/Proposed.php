<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Proposed extends Model
{
    protected $table = 'proposed';
    protected $fillable = ['id', 'applicant_id', 'application_letter', 'category_d'];
    public $incrementing = false;

    public function berkas()
    {
        return $this->belongsToMany('App\Models\File');
    }

    public function category()
    {
        return $this->belongsTo('App\Models\Category', 'category_id');
    }

    public function applicant()
    {
        return $this->belongsTo('App\Models\Applicant', 'applicant_id');
    }
}
