@if(session('message')!='')
    <div class="alert alert-primary" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <b>Message : </b>{{ session('message')}}
    </div>
@endif
