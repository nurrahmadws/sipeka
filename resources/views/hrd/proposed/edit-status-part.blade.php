<div class="card card-primary card-outline email" style="display:none;">
    <div class="card-header">
        <h3 class="card-title">Perbaharui Status</h3>
        <button aria-hidden="true" class="close" onclick="closeEmail()" type="button">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <form id="frm_edit_status">
        <div class="card-body">
            <span id="form_result"></span>
            {!! $detail->application_letter !!}
            <table class="table">
                <tr>
                    <th width="30%">Kategori Pekerjaan Yang Dipilih</th>
                    <td>:</td>
                    <td>{{ $detail->category->name }}</td>
                </tr>
                <tr>
                    <th>Berkas Yang Disertakan</th>
                    <td>:</td>
                    <td>
                        <div class="row">
                            @foreach ($detail->berkas as $item)
                            <div class="col-md-3">
                                <a href="{{asset('berkas/'.$item->file)}}" target="__blank"><img src="{{asset('berkas/'.$item->file)}}" class="img-thumbnail img"></a>
                                {{$item->title}}
                            </div>
                            @endforeach
                        </div>
                    </td>
                </tr>
                <tr>
                    <th width="30%">Status</th>
                    <td>:</td>
                    <td>
                        <select name="status" class="form-control select2bs4" required>
                            @if ($detail->status == 'Menunggu Konfirmasi')
                            <option value="{{$detail->status}}" selected>{{$detail->status}}</option>
                            <option value="Tolak">Tolak</option>
                            <option value="Terima">Terima</option>
                            <option value="Undang Wawancara">Undang Wawancara</option>
                            @elseif($detail->status == 'Tolak')
                            <option value="{{$detail->status}}" selected>{{$detail->status}}</option>
                            <option value="Menunggu Konfirmasi">Menunggu Konfirmasi</option>
                            <option value="Terima">Terima</option>
                            <option value="Undang Wawancara">Undang Wawancara</option>
                            @elseif($detail->status == 'Terima')
                            <option value="{{$detail->status}}" selected>{{$detail->status}}</option>
                            <option value="Menunggu Konfirmasi">Menunggu Konfirmasi</option>
                            <option value="Tolak">Tolak</option>
                            <option value="Undang Wawancara">Undang Wawancara</option>
                            @elseif($detail->status == 'Undang Wawancara')
                            <option value="{{$detail->status}}" selected>{{$detail->status}}</option>
                            <option value="Menunggu Konfirmasi">Menunggu Konfirmasi</option>
                            <option value="Tolak">Tolak</option>
                            <option value="Terima">Terima</option>
                            @endif
                        </select>
                    </td>
                </tr>
                <tr>
                    <th width="30%">Keterangan</th>
                    <td>:</td>
                    <td>
                        <textarea name="description" class="form-control" id="textarea">{!!$detail->description!!}</textarea>
                    </td>
                </tr>
            </table>
        </div>
        <div class="card-footer">
            <input type="hidden" name="id" value="{{$detail->id}}">
            <input type="submit" value="Kirim" class="btn btn-primary btn-flat btn-block go">
        </div>
    </form>
</div>
