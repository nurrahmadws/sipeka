{!! $detail->application_letter !!}
<table class="table">
    <tr>
        <th width="30%">Kategori Pekerjaan Yang Dipilih</th>
        <td>:</td>
        <td>{{ $detail->category->name }}</td>
    </tr>
    <tr>
        <th>Berkas Yang Disertakan</th>
        <td>:</td>
        <td>
            <div class="row">
                @foreach ($detail->berkas as $item)
                <div class="col-md-3">
                    <a href="{{asset('berkas/'.$item->file)}}" target="__blank"><img src="{{asset('berkas/'.$item->file)}}" class="img-thumbnail img"></a>
                    {{$item->title}}
                </div>
                @endforeach
            </div>
        </td>
    </tr>
    <tr>
        <th width="30%">Status</th>
        <td>:</td>
        <td>{{ $detail->status }}</td>
    </tr>
    <tr>
        <th width="30%">Keterangan</th>
        <td>:</td>
        <td>{!! $detail->description !!}</td>
    </tr>
</table>
<div class="card-footer">
    <a onclick="ShowAddPart()" class="btn btn-sm bg-navy btn-flat btn-block">Perbaharui Status</a>
</div>
