@extends('hrd.layout')
@section('title')
    Proposed
@endsection
@section('css')

@endsection
@section('content')
<br><br><br>
<div class="row">
    <div class="col-md-10 offset-1">
        <div class="card card-primary card-outline">
            <div class="card-header">
                Semua Lamaran Masuk
            </div>
            <div class="card-body">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Pelamar</th>
                            <th>Kategori Pekerjaan</th>
                            <th>Tanggal Daftar</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($proposeds as $item)
                        <tr>
                            <td>{{$loop->iteration}}</td>
                            <td>{{$item->applicant->name}}</td>
                            <td>{{$item->category->name}}</td>
                            <td>{{\Carbon\Carbon::parse($item->created_at)->format('d F Y')}}</td>
                            <td>
                                <a href="{{url('hrd/proposed/'.$item->id.'/detail')}}" class="btn bg-navy btn-flat">Lihat</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
@push('js')

@endpush
