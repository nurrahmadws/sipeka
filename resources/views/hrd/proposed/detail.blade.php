@extends('hrd.layout')
@section('title')
    Detail Lamaran
@endsection
@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('backend/css/select2.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('backend/css/select2-bootstrap4.min.css') }}">
<link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-bs4.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
@endsection
@section('content')
<br><br><br>
<div class="row">
    <div class="col-md-10 offset-1">
        <div class="card card-info card-outline data">
            <div class="card-header p-2">
                <ul class="nav nav-pills">
                    <li class="nav-item">
                        <a href="#dataPelamar" class="nav-link active" data-toggle="tab">Surat Lamaran</a>
                    </li>
                    <li class="nav-item">
                        <a href="#biodataPelamar" class="nav-link" data-toggle="tab">Biodata Pelamar</a>
                    </li>
                    <li class="nav-item">
                        <a href="#riwayatPendidikanPelamar" class="nav-link" data-toggle="tab">Riwayat Pendidikan</a>
                    </li>
                </ul>
            </div>
            <div class="card-body">
                <div class="tab-content">
                    <div class="active tab-pane" id="dataPelamar">
                        @include('hrd.proposed.surat-pelamar')
                    </div>
                    <div class="tab-pane" id="biodataPelamar">
                        @include('hrd.proposed.biodata-pelamar')
                    </div>
                    <div class="tab-pane" id="riwayatPendidikanPelamar">
                        @include('hrd.proposed.riwayat-pendidikan')
                    </div>
                </div>
            </div>
        </div>
        @include('hrd.proposed.edit-status-part')
    </div>
</div>
@endsection
@push('js')
<script src="{{ asset('frontend/js/select2.full.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-bs4.js"></script>
<script>
$('.select2bs4').select2({
      theme: 'bootstrap4'
});
$('#textarea').summernote({
    placeholder: 'Tulislah balasan dengan kata yang merupakan bahasa baku menurut Kamus Besar Bahasa Indonesia. Gunakanlah kata-kata yang mudah dipahami, singkat, padat, jelas, sopan, dan menarik.',
    tabsize: 2,
    height: 300
});
function ShowAddPart()
{
    $(document).ready(function(){
        $('.data').hide();
        $('.email').slideToggle();
    });
}

function closeEmail()
{
    $(document).ready(function(){
        $('.data').fadeIn();
        $('.email').fadeOut();
    });
}

$(document).ready(function(){
    $("#frm_edit_status").on('submit', function(e){
        e.preventDefault();
        $.ajaxSetup({
            headers:{
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url: "/hrd/update-proposed/" + $("#frm_edit_status input[name=id]").val(),
            data: new FormData(this),
            contentType: false,
            cache:false,
            processData: false,
            dataType: "json",
            beforeSend: function(){
                $('.go').addClass('btn-progress');
            },
            success: function (response) {

                var html = '';
                if(response.errors){
                    html = '<div class="alert alert-danger">';
                    for(var count = 0; count < response.errors.length; count++)
                    {
                        html += '<p>' + response.errors[count] + "</p>";
                    }
                    html += '</div>';
                    $('.go').removeClass('btn-progress');
                }
                if (response.success) {
                    html = '<div class="alert alert-success"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + response.success + '</div>';
                    $('#frm_edit_status')[0].reset();
                    setTimeout(function(){
                        location.reload();
                    }, 1000);
                }
                $('#form_result').html(html);
            }
        });
    });
});
</script>
@endpush
