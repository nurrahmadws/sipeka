<table class="table">
    <tr>
        <th>Nama Lengkap</th>
        <td>:</td>
        <td>{{$applicant->name}}</td>
    </tr>
    <tr>
        <th>Tempat, Tanggal Lahir</th>
        <td>:</td>
        <td>{{$applicant->date_place}}, {{\Carbon\Carbon::parse($applicant->date_birth)->format('d F Y')}}</td>
    </tr>
    <tr>
        <th>Jenis Kelamin</th>
        <td>:</td>
        <td>{{$applicant->gender}}</td>
    </tr>
    <tr>
        <th>Agama</th>
        <td>:</td>
        <td>{{$applicant->religion}}</td>
    </tr>
    <tr>
        <th>Pendidikan Terakhir</th>
        <td>:</td>
        <td>{{$applicant->last_education}}</td>
    </tr>
    <tr>
        <th>Alamat (Berdasarkan KTP)</th>
        <td>:</td>
        <td>{{$applicant->address_based_id_card}}</td>
    </tr>
    <tr>
        <th>Alamat Sekarang</th>
        <td>:</td>
        <td>{{$applicant->current_address}}</td>
    </tr>
    <tr>
        <th>No Handphone</th>
        <td>:</td>
        <td>{{$applicant->phone}}</td>
    </tr>
    <tr>
        <th>Email</th>
        <td>:</td>
        <td>{{$applicant->users->email}}</td>
    </tr>
    <tr>
        <th>Kartu Identitas</th>
        <td>:</td>
        <td>{{$applicant->identity}}</td>
    </tr>
    <tr>
        <th>Nomor Kartu Identitas</th>
        <td>:</td>
        <td>{{$applicant->id_identity}}</td>
    </tr>
    <tr>
        <th>Status Pernikahan</th>
        <td>:</td>
        <td>{{$applicant->marital_status}}</td>
    </tr>
    <tr>
        <th>Photo</th>
        <td>:</td>
        <td><img src="{{asset('image/'.$applicant->photo)}}" class="img-thumbnail"></td>
    </tr>
</table>
