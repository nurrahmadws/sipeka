<table class="table table-bordered table-hover">
    <thead>
        <tr>
            <th class="alert alert-info" colspan="4" style="text-align:center;">Data Sekolah Dasar</th>
        </tr>
    </thead>
    <thead>
        <tr>
            <th style="text-align:center;">Nama Sekolah</th>
            <th style="text-align:center;">Tahun Masuk</th>
            <th style="text-align:center;">Tahun Lulus  </th>
        </tr>
    </thead>
    <tbody>
        @if (isset($sd))
        <tr>
            <td style="text-align:center;">{{$sd->name}}</td>
            <td style="text-align:center;">{{$sd->entry_year}}</td>
            <td style="text-align:center;">{{$sd->graduation_year}}</td>
        </tr>
        @else
        <tr>
            <td colspan="4" style="text-align:center;">Kosong</td>
        </tr>
        @endif
    </tbody>
    <thead>
        <tr>
            <th class="alert alert-info" colspan="4" style="text-align:center;">Data Sekolah Menengah Pertama</th>
        </tr>
    </thead>
    <thead>
        <tr>
            <th style="text-align:center;">Nama Sekolah</th>
            <th style="text-align:center;">Tahun Masuk</th>
            <th style="text-align:center;">Tahun Lulus  </th>
        </tr>
    </thead>
    <tbody>
        @if (isset($smp))
        <tr>
            <td style="text-align:center;">{{$smp->name}}</td>
            <td style="text-align:center;">{{$smp->entry_year}}</td>
            <td style="text-align:center;">{{$smp->graduation_year}}</td>
        </tr>
        @else
        <tr>
            <td colspan="4" style="text-align:center;">Kosong</td>
        </tr>
        @endif
    </tbody>
    <thead>
        <tr>
            <th class="alert alert-info" colspan="4" style="text-align:center;">Data Sekolah Menengah Atas/Kejuruan</th>
        </tr>
    </thead>
    <thead>
        <tr>
            <th style="text-align:center;">Nama Sekolah</th>
            <th style="text-align:center;">Tahun Masuk</th>
            <th style="text-align:center;">Tahun Lulus  </th>
        </tr>
    </thead>
    <tbody>
        @if (isset($smksma))
        <tr>
            <td style="text-align:center;">{{$smksma->name}}</td>
            <td style="text-align:center;">{{$smksma->entry_year}}</td>
            <td style="text-align:center;">{{$smksma->graduation_year}}</td>
        </tr>
        @else
        <tr>
            <td colspan="4" style="text-align:center;">Kosong</td>
        </tr>
        @endif
    </tbody>
    <thead>
        <tr>
            <th class="alert alert-info" colspan="4" style="text-align:center;">Data Strata 1 (S1)</th>
        </tr>
    </thead>
    <thead>
        <tr>
            <th style="text-align:center;">Nama Universitas/Institusi</th>
            <th style="text-align:center;">Tahun Masuk</th>
            <th style="text-align:center;">Tahun Lulus  </th>
        </tr>
    </thead>
    <tbody>
        @if (isset($strata_1))
        <tr>
            <td style="text-align:center;">{{$strata_1->name}}</td>
            <td style="text-align:center;">{{$strata_1->entry_year}}</td>
            <td style="text-align:center;">{{$strata_1->graduation_year}}</td>
        </tr>
        @else
        <tr>
            <td colspan="4" style="text-align:center;">Kosong</td>
        </tr>
        @endif
    </tbody>
    <thead>
        <tr>
            <th class="alert alert-info" colspan="4" style="text-align:center;">Data Pasca Sarjana</th>
        </tr>
    </thead>
    <thead>
        <tr>
            <th style="text-align:center;">Nama Universitas/Institusi</th>
            <th style="text-align:center;">Tahun Masuk</th>
            <th style="text-align:center;">Tahun Lulus  </th>
        </tr>
    </thead>
    <tbody>
        @if (isset($pasca_sarjana))
        <tr>
            <td style="text-align:center;">{{$pasca_sarjana->name}}</td>
            <td style="text-align:center;">{{$pasca_sarjana->entry_year}}</td>
            <td style="text-align:center;">{{$pasca_sarjana->graduation_year}}</td>
        </tr>
        @else
        <tr>
            <td colspan="4" style="text-align:center;">Kosong</td>
        </tr>
        @endif
    </tbody>
    <thead>
        <tr>
            <th class="alert alert-info" colspan="4" style="text-align:center;">Data Paket C</th>
        </tr>
    </thead>
    <thead>
        <tr>
            <th style="text-align:center;">Nama Lembaga</th>
            <th style="text-align:center;">Tahun Masuk</th>
            <th style="text-align:center;">Tahun Lulus  </th>
        </tr>
    </thead>
    <tbody>
        @if (isset($paket_c))
        <tr>
            <td style="text-align:center;">{{$paket_c->name}}</td>
            <td style="text-align:center;">{{$paket_c->entry_year}}</td>
            <td style="text-align:center;">{{$paket_c->graduation_year}}</td>
        </tr>
        @else
        <tr>
            <td colspan="4" style="text-align:center;">Kosong</td>
        </tr>
        @endif
    </tbody>
</table>
