<div class="modal fade" id="edit_category_job">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Edit Pekerjaan</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
            </div>
            <form id="frmEditCategoryJob">
                <div class="modal-body">
                    <span id="form_result_edit"></span>
                    <div class="form-group">
                        <label>Nama Pekerjaan</label>
                        <input type="text" name="name" class="form-control" placeholder="Cth: Web developer">
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="id">
                    <input type="submit" value="Simpan" class="btn bg-maroon btn-flat go">
                </div>
            </form>
        </div>
    </div>
</div>
