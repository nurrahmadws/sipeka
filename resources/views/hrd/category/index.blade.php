@extends('hrd.layout')
@section('title')
    Kategori Pekerjaan
@endsection
@section('content')
<br>
<br>
<div class="row">
    <div class="col-md-10 offset-1">
        @include('alert')
        <div class="card card-info card-outline">
            <div class="card-header">
                <div class="row">
                    <div class="col-md-6">
                        Kategori Pekerjaan Yang Dibuka
                    </div>
                    <div class="col-md-5 text-right" style="margin-left:72px">
                        <a onclick="addCategoryJob()" class="btn btn-primary btn-flat btn-xs"><i class="fa fa-plus"></i> Kategori Pekerjaan</a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <table class="table table-bordered table-hover" id="jobTable">
                    <thead>
                        <tr>
                            <th style="text-align:center;">No</th>
                            <th style="text-align:center;">Nama Pekerjaan</th>
                            <th style="text-align:center;">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($categories as $item)
                        <tr>
                            <td style="text-align:center;">{{$loop->iteration}}</td>
                            <td style="text-align:center;">{{$item->name}}</td>
                            <td style="text-align:center;">
                                <a onclick="editCategory('{{$item->id}}')" class="btn bg-navy btn-flat btn-sm">Edit</a>
                                <a onclick="deleteCategory('{{$item->id}}')" class="btn bg-maroon btn-flat btn-sm">Delete</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@include('hrd.category.modal-add')
@include('hrd.category.modal-edit')
@include('hrd.category.modal-delete')
@endsection
@push('js')
<script src="{{ asset('backend/js/jquery.dataTables.js') }}"></script>
<script src="{{ asset('backend/js/dataTables.bootstrap4.js') }}"></script>
<script src="{{ asset('backend/data/category.js') }}"></script>
@endpush
