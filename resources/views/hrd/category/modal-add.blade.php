<div class="modal fade" id="add_category_job">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Tambah Kategori Pekerjaan</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
            </div>
            <form id="frmAddCategoryJob">
                <div class="modal-body">
                    <span id="form_result"></span>
                    <div class="form-group">
                        <label>Nama Pekerjaan</label>
                        <input type="text" name="name" class="form-control" placeholder="Cth: Web developer">
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="submit" value="Simpan" class="btn bg-navy btn-flat go">
                </div>
            </form>
        </div>
    </div>
</div>
