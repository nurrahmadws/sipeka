@extends('applicants.layout')
@section('title')
    Lamaran Saya
@endsection
@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('backend/css/select2.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('backend/css/select2-bootstrap4.min.css') }}">
<link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-bs4.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
@endsection
@section('content')
<br>
<br>
<br>
<div class="row">
    <div class="col-md-10 offset-1">
        @include('alert')
        <div class="card card-primary card-outline data">
            <div class="card-header">
                <div class="row">
                    <div class="col-md-6">
                        <h3 class="card-title">Lamaran Saya</h3>
                    </div>
                    <div class="col-md-5 text-right" style="margin-left:72px">
                        <a onclick="ShowAddPart()" class="btn btn-sm bg-maroon btn-flat">Masukkan Lamaran</a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <table class="table table-bordered table-hover" id="myTable">
                    <thead>
                        <tr>
                            <th style="text-align:center;">No</th>
                            <th style="text-align:center;">Kategori Pekerjaan Yang Dilamar</th>
                            <th style="text-align:center;">Tanggal Daftar</th>
                            <th style="text-align:center;">Status</th>
                            <th style="text-align:center;">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($proposeds as $item)
                        <tr>
                            <td style="text-align:center;">{{$loop->iteration}}</td>
                            <td style="text-align:center;">{{$item->category->name}}</td>
                            <td style="text-align:center;">{{\Carbon\Carbon::parse($item->created_at)->format('d F Y')}}</td>
                            <td style="text-align:center;">{{$item->status}}</td>
                            <td style="text-align:center;">
                                <a href="{{ url('applicant/lamaran/'.$item->id) }}" class="btn bg-navy btn-sm btn-flat">Lihat</a>
                                <a href="{{ url('applicant/lamaran/'.$item->id.'/edit') }}" class="btn btn-warning btn-sm btn-flat">Edit</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="apply_lamaran">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Masukkan Lamaran Pekerjaan</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
            </div>
            <form id="frmApplyLamaran">
                <div class="modal-body">
                    <span id="form_result"></span>
                    <div class="form-group">
                        <label>Pilih Kategori Pekerjaan</label><br>
                        <select name="category_id" class="form-control select2bs4">
                            @foreach ($categories as $item)
                                <option value="{{$item->id}}">{{$item->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Buat Surat Lamaran</label>
                        <textarea name="application_letter" id="textarea" class="form-control"></textarea>
                    </div>
                    <div class="form-group">
                        <label>Pilih Berkas Anda</label><br>
                        <select name="berkas[]" class="form-control select2bs4" multiple="multiple" required>
                            @foreach ($berkas as $item)
                                <option value="{{$item->id}}">{{$item->title}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="submit" value="Kirim" class="btn btn-flat bg-navy go">
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@push('js')
<script src="{{ asset('frontend/js/jquery.dataTables.js') }}"></script>
<script src="{{ asset('frontend/js/dataTables.bootstrap4.js') }}"></script>
<script src="{{ asset('frontend/js/select2.full.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-bs4.js"></script>
<script>
$('.select2bs4').select2({
      theme: 'bootstrap4'
});

$('#textarea').summernote({
    placeholder: 'Tulislah surat lamaran kerja dengan kata yang merupakan bahasa baku menurut Kamus Besar Bahasa Indonesia. Gunakanlah kata-kata yang mudah dipahami, singkat, padat, jelas, sopan, dan menarik.',
    tabsize: 2,
    height: 100
});

$(function(){
    $("#myTable").DataTable();
});

function ShowAddPart()
{
    $("#apply_lamaran").modal('show');
}

$(document).ready(function(){
    $("#frmApplyLamaran").on('submit', function(e){
        e.preventDefault();
        $.ajaxSetup({
            headers:{
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url: "/applicant/lamaran",
            data: new FormData(this),
            contentType: false,
            cache:false,
            processData: false,
            dataType: "json",
            beforeSend: function(){
                $('.go').addClass('btn-progress');
            },
            success: function (response) {

                var html = '';
                if(response.errors){
                    html = '<div class="alert alert-danger">';
                    for(var count = 0; count < response.errors.length; count++)
                    {
                        html += '<p>' + response.errors[count] + "</p>";
                    }
                    html += '</div>';
                    $('.go').removeClass('btn-progress');
                }
                if (response.success) {
                    html = '<div class="alert alert-success"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + response.success + '</div>';
                    $('#frmApplyLamaran')[0].reset();
                    setTimeout(function(){
                        location.reload();
                    }, 1000);
                }
                $('#form_result').html(html);
            }
        });
    });
});
</script>
@endpush
