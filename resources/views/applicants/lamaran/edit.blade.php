@extends('applicants.layout')
@section('title')
    Edit Lamaran {{$edit->category->name}}
@endsection
@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('backend/css/select2.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('backend/css/select2-bootstrap4.min.css') }}">
<link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-bs4.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
@endsection
@section('content')
<br><br><br>
<div class="row">
    <div class="col-md-10 offset-1">
        <div class="card card-danger card-outline">
            <div class="card-header">
                <div class="row">
                    <div class="col-md-6">
                        <h3 class="card-title"><a href="{{url('applicant/lamaran')}}"><i class="fa fa-chevron-left"></i></a> Lamaran Saya</h3>
                    </div>
                </div>
            </div>
            <div class="card-body">
                @include('alert')
                <form action="{{url('applicant/lamaran/'.$edit->id.'/update')}}" method="POST">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Pilih Kategori Pekerjaan</label><br>
                            <select name="category_id" class="form-control select2bs4">
                                @foreach ($categories as $item)
                                    <option value="{{$item->id}}"
                                        @if (isset($edit))
                                            {{$item->id == $edit->category_id ? 'selected' : ''}}
                                        @endif>{{$item->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Buat Surat Lamaran</label>
                            <textarea name="application_letter" id="textarea" class="form-control">
                                {!! $edit->application_letter !!}
                            </textarea>
                        </div>
                        <div class="form-group">
                            <label>Pilih Berkas Anda</label><br>
                            <select name="berkas[]" class="form-control select2bs4" multiple="multiple" required>
                                @foreach ($berkas as $item)
                                    <option value="{{$item->id}}"
                                        @foreach ($edit->berkas as $itemm)
                                            {{$item->id == $itemm->id ? 'selected' : ''}}
                                        @endforeach>{{$item->title}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="submit" value="Kirim" class="btn btn-flat bg-navy go">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@push('js')
<script src="{{ asset('frontend/js/jquery.dataTables.js') }}"></script>
<script src="{{ asset('frontend/js/dataTables.bootstrap4.js') }}"></script>
<script src="{{ asset('frontend/js/select2.full.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-bs4.js"></script>
<script>
$('.select2bs4').select2({
      theme: 'bootstrap4'
});

$('#textarea').summernote({
    placeholder: 'Tulislah surat lamaran kerja dengan kata yang merupakan bahasa baku menurut Kamus Besar Bahasa Indonesia. Gunakanlah kata-kata yang mudah dipahami, singkat, padat, jelas, sopan, dan menarik.',
    tabsize: 2,
    height: 100
});
</script>
@endpush
