@extends('applicants.layout')
@section('title')
    {{$detail->category->name}}
@endsection
@section('css')
<style>
    .img{
        display: block;
        margin-left: auto;
        margin-right: auto;
        width:200px;
        height:200px;
        object-fit: cover;
    }
</style>
@endsection
@section('content')
<br>
<br>
<br>
    <div class="row">
        <div class="col-md-10 offset-1">
            @include('alert')
            <div class="card card-warning card-outline">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-6">
                            <h3 class="card-title"><a href="{{url('applicant/lamaran')}}"><i class="fa fa-chevron-left"></i></a> Lamaran Saya</h3>
                        </div>
                        <div class="col-md-5 text-right" style="margin-left:72px">
                            <a href="{{ url('applicant/lamaran/'.$detail->id.'/edit') }}" class="btn btn-sm bg-maroon btn-flat">Edit Lamaran</a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <table class="table">
                        <tr>
                            <th>Kategori Pekerjaan</th>
                            <td>:</td>
                            <td>{{$detail->category->name}}</td>
                        </tr>
                        <tr>
                            <th>Surat Lamaran</th>
                            <td>:</td>
                            <td>{!! $detail->application_letter !!}</td>
                        </tr>
                        <tr>
                            <th>Berkas Yang Dilampirkan</th>
                            <td>:</td>
                            <td>
                                <div class="row">
                                    @foreach ($detail->berkas as $item)
                                    <div class="col-md-3">
                                        <a href="{{asset('berkas/'.$item->file)}}" target="__blank"><img src="{{asset('berkas/'.$item->file)}}" class="img-thumbnail img"></a>
                                        {{$item->title}}
                                    </div>
                                    @endforeach
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>Status</th>
                            <td>:</td>
                            <td>{!! $detail->status !!}</td>
                        </tr>
                        <tr>
                            <th>Keterangan</th>
                            <td>:</td>
                            <td>{!! $detail->description !!}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
