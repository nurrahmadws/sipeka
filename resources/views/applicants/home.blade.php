@extends('applicants.layout')
@section('title')
Home
@endsection
@section('content')
<div class="row">
    <div class="col-md-10 offset-1">
        @if($errors->any())
        <div class="alert alert-primary mt-3" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <b>Message : </b>{{$errors->first()}}
        </div>
        @endif
        <div class="card mt-5">
            <div class="card-header">
                <h3 class="card-title">Selamat Datang di Aplikasi Sistem Pendaftaran Karyawan Sederhana</h3>
            </div>
            <div class="card-body">
                <ol>
                    <li>Isilah Biodata Anda Terlebih Dahulu Sebelum Mengakses Menu yang Lain</li>
                    <li>Pastikan Data Yang Anda Isi Sudah Benar Dan Sesuai</li>
                    <li>Pada Saat Ingin Mengisi Data Riwayat Pendidikan, Isilah Sesuai Dengan Riwayat Kualifikasi Pendidikan Anda, Tidak Perlu Diisi Semua</li>
                    <li>Untuk Berkas, File Yang di Upload Harus Berupa Gambar Yang Mendukung Ekstensi (.jpg, .png, .webp) dan ekstensi format gambar yang lain</li>
                    <li>Apply Lamaran yang Sesuai dengan Bidang dan Kemampuan Anda</li>
                    <li>Anda Boleh Memasukkan Lamaran Lebih Dari Satu</li>
                </ol>
            </div>
        </div>
    </div>
</div>
@endsection
