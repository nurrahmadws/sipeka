<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Sipeka</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('backend/css/fontawesome/css/all.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('backend/css/adminlte.min.css') }}">
    <link rel="stylesheet" href="{{asset('logreg/css/material-design-iconic-font.min.css')}}">
    <link rel="stylesheet" href="{{asset('logreg/css/style.css')}}">
</head>
<body>
<div class="main">
<!-- Sing in  Form -->
<section class="signup">
    <div class="container">
        <div class="signup-content">
            <div class="signup-form">
                @include('alert')
                <h2 class="form-title">Register</h2>
                <form method="POST" class="register-form" id="register-form" action="{{url('applicant/register-process')}}">
                    @csrf
                    <div class="form-group">
                        <label><i class="fas fa-user"></i></label>
                        <input type="text" name="name" id="name" placeholder="Masukkan Nama Lengkap Anda"/>
                        <span class="text-danger">
                            {{ $errors->first('name') }}
                        </span>
                    </div>
                    <div class="form-group">
                        <label for="email"><i class="fas fa-envelope"></i></label>
                        <input type="email" name="email" id="email" placeholder="Email Anda"/>
                        <span class="text-danger">
                            {{ $errors->first('email') }}
                        </span>
                    </div>
                    <div class="form-group">
                        <label for="pass"><i class="fas fa-lock"></i></label>
                        <input type="password" name="password" id="password" placeholder="Password"/>
                        <span class="text-danger">
                            {{ $errors->first('password') }}
                        </span>
                    </div>
                    <div class="form-group">
                        <label for="re-pass"><i class="fas fa-lock"></i></label>
                        <input type="password" name="confirm_password" id="confirm_password" placeholder="Ulangi Password Anda"/>
                        <span class="text-danger">
                            {{ $errors->first('confirm_password') }}
                        </span>
                    </div>
                    <div class="form-group form-button">
                        <input type="submit" name="signup" id="signup" class="form-submit" value="Register"/>
                    </div>
                </form>
            </div>
            <div class="signup-image">
                <figure><img src="{{asset('logreg/images/signin-image.jpg')}}" alt="sing up image"></figure>
                <a href="{{url('applicant/login')}}" class="signup-image-link">Saya Sudah Memiliki Akun</a>
            </div>
        </div>
    </div>
</section>
</div>
<script src="{{asset('logreg/js/jquery.min.js')}}"></script>
<script src="{{asset('logreg/js/main.js')}}"></script>
</body>
</html>
