@if (isset($strata_1))
<div class="card card-info">
    <div class="card-header">
        <h4 class="card-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
                Strata 1
            </a>
        </h4>
    </div>
    <div id="collapseFour" class="panel-collapse collapse">
        <form id="frmS1">
            <div class="card-body">
                @include('alert')
                <span id="form_result_s1"></span>
                <div class="form-group">
                    <label>Nama Universitas/Institusi</label>
                    <input type="text" name="name" class="form-control" disabled placeholder="Cth: Universitas Mulawarman">
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            <label>Tahun Masuk</label>
                            <input type="number" name="entry_year" class="form-control" disabled placeholder="Cth: 2010">
                        </div>
                        <div class="col-md-6">
                            <label>Tahun Lulus</label>
                            <input type="number" name="graduation_year" class="form-control" disabled placeholder="Cth: 2019">
                        </div>
                    </div>
                </div>
                <input type="hidden" name="type" value="strata_1">
                <input type="submit" value="Simpan" class="btn bg-navy btn-flat go" disabled>
            </div>
        </form>
    </div>
</div>
@else
<div class="card card-info">
    <div class="card-header">
        <h4 class="card-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
                Strata 1
            </a>
        </h4>
    </div>
    <div id="collapseFour" class="panel-collapse collapse">
        <form id="frmS1">
            <div class="card-body">
                @include('alert')
                <span id="form_result_s1"></span>
                <div class="form-group">
                    <label>Nama Universitas/Institusi</label>
                    <input type="text" name="name" class="form-control" placeholder="Cth: Universitas Mulawarman">
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            <label>Tahun Masuk</label>
                            <input type="number" name="entry_year" class="form-control" placeholder="Cth: 2010">
                        </div>
                        <div class="col-md-6">
                            <label>Tahun Lulus</label>
                            <input type="number" name="graduation_year" class="form-control" placeholder="Cth: 2019">
                        </div>
                    </div>
                </div>
                <input type="hidden" name="type" value="strata_1">
                <input type="submit" value="Simpan" class="btn bg-navy btn-flat go">
            </div>
        </form>
    </div>
</div>
@endif
