@extends('applicants.layout')
@section('title')
Pendidikan Saya
@endsection
@section('css')
@endsection
@section('content')
<div class="row">
    <div class="col-md-10 offset-1 mt-5 mb-5">
        @include('alert')
        {{-- @if (isset($educations)) --}}
            <div class="card card-info card-outline info">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-6">
                            <h3 class="card-title">Data Pendidikan Saya</h3>
                        </div>
                        <div class="col-md-5 text-right" style="margin-left:70px;">
                            <a onclick="showAddPart()" class="btn btn-flat bg-maroon btn-xs"><i class="fas fa-plus"></i> Data Pendidikan</a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th class="alert alert-info" colspan="4" style="text-align:center;">Data Sekolah Dasar</th>
                            </tr>
                        </thead>
                        <thead>
                            <tr>
                                <th style="text-align:center;">Nama Sekolah</th>
                                <th style="text-align:center;">Tahun Masuk</th>
                                <th style="text-align:center;">Tahun Lulus  </th>
                                <th style="text-align:center;">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (isset($sd))
                                <tr>
                                    <td style="text-align:center;">{{$sd->name}}</td>
                                    <td style="text-align:center;">{{$sd->entry_year}}</td>
                                    <td style="text-align:center;">{{$sd->graduation_year}}</td>
                                    <td style="text-align:center;">
                                        <a onclick="editPendidikan('{{$sd->id}}')" class="btn btn-sm btn-warning btn-flat">Edit</a>
                                        <a onclick="deletePendidikan('{{$sd->id}}')" class="btn btn-sm btn-danger btn-flat">Hapus</a>
                                    </td>
                                </tr>
                            @else
                                <tr>
                                    <td colspan="4" style="text-align:center;">Kosong</td>
                                </tr>
                            @endif
                        </tbody>
                        <thead>
                            <tr>
                                <th class="alert alert-info" colspan="4" style="text-align:center;">Data Sekolah Menengah Pertama</th>
                            </tr>
                        </thead>
                        <thead>
                            <tr>
                                <th style="text-align:center;">Nama Sekolah</th>
                                <th style="text-align:center;">Tahun Masuk</th>
                                <th style="text-align:center;">Tahun Lulus  </th>
                                <th style="text-align:center;">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (isset($smp))
                                <tr>
                                    <td style="text-align:center;">{{$smp->name}}</td>
                                    <td style="text-align:center;">{{$smp->entry_year}}</td>
                                    <td style="text-align:center;">{{$smp->graduation_year}}</td>
                                    <td style="text-align:center;">
                                        <a onclick="editPendidikan('{{$smp->id}}')" class="btn btn-sm btn-warning btn-flat">Edit</a>
                                        <a onclick="deletePendidikan('{{$smp->id}}')" class="btn btn-sm btn-danger btn-flat">Hapus</a>
                                    </td>
                                </tr>
                            @else
                                <tr>
                                    <td colspan="4" style="text-align:center;">Kosong</td>
                                </tr>
                            @endif
                        </tbody>
                        <thead>
                            <tr>
                                <th class="alert alert-info" colspan="4" style="text-align:center;">Data Sekolah Menengah Atas/Kejuruan</th>
                            </tr>
                        </thead>
                        <thead>
                            <tr>
                                <th style="text-align:center;">Nama Sekolah</th>
                                <th style="text-align:center;">Tahun Masuk</th>
                                <th style="text-align:center;">Tahun Lulus  </th>
                                <th style="text-align:center;">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (isset($smksma))
                                <tr>
                                    <td style="text-align:center;">{{$smksma->name}}</td>
                                    <td style="text-align:center;">{{$smksma->entry_year}}</td>
                                    <td style="text-align:center;">{{$smksma->graduation_year}}</td>
                                    <td style="text-align:center;">
                                        <a onclick="editPendidikan('{{$smksma->id}}')" class="btn btn-sm btn-warning btn-flat">Edit</a>
                                        <a onclick="deletePendidikan('{{$smksma->id}}')" class="btn btn-sm btn-danger btn-flat">Hapus</a>
                                    </td>
                                </tr>
                            @else
                                <tr>
                                    <td colspan="4" style="text-align:center;">Kosong</td>
                                </tr>
                            @endif
                        </tbody>
                        <thead>
                            <tr>
                                <th class="alert alert-info" colspan="4" style="text-align:center;">Data Strata 1 (S1)</th>
                            </tr>
                        </thead>
                        <thead>
                            <tr>
                                <th style="text-align:center;">Nama Universitas/Institusi</th>
                                <th style="text-align:center;">Tahun Masuk</th>
                                <th style="text-align:center;">Tahun Lulus  </th>
                                <th style="text-align:center;">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (isset($strata_1))
                                <tr>
                                    <td style="text-align:center;">{{$strata_1->name}}</td>
                                    <td style="text-align:center;">{{$strata_1->entry_year}}</td>
                                    <td style="text-align:center;">{{$strata_1->graduation_year}}</td>
                                    <td style="text-align:center;">
                                        <a onclick="editPendidikan('{{$strata_1->id}}')" class="btn btn-sm btn-warning btn-flat">Edit</a>
                                        <a onclick="deletePendidikan('{{$strata_1->id}}')" class="btn btn-sm btn-danger btn-flat">Hapus</a>
                                    </td>
                                </tr>
                            @else
                                <tr>
                                    <td colspan="4" style="text-align:center;">Kosong</td>
                                </tr>
                            @endif
                        </tbody>
                        <thead>
                            <tr>
                                <th class="alert alert-info" colspan="4" style="text-align:center;">Data Pasca Sarjana</th>
                            </tr>
                        </thead>
                        <thead>
                            <tr>
                                <th style="text-align:center;">Nama Universitas/Institusi</th>
                                <th style="text-align:center;">Tahun Masuk</th>
                                <th style="text-align:center;">Tahun Lulus  </th>
                                <th style="text-align:center;">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (isset($pasca_sarjana))
                                <tr>
                                    <td style="text-align:center;">{{$pasca_sarjana->name}}</td>
                                    <td style="text-align:center;">{{$pasca_sarjana->entry_year}}</td>
                                    <td style="text-align:center;">{{$pasca_sarjana->graduation_year}}</td>
                                    <td style="text-align:center;">
                                        <a onclick="editPendidikan('{{$pasca_sarjana->id}}')" class="btn btn-sm btn-warning btn-flat">Edit</a>
                                        <a onclick="deletePendidikan('{{$pasca_sarjana->id}}')" class="btn btn-sm btn-danger btn-flat">Hapus</a>
                                    </td>
                                </tr>
                            @else
                                <tr>
                                    <td colspan="4" style="text-align:center;">Kosong</td>
                                </tr>
                            @endif
                        </tbody>
                        <thead>
                            <tr>
                                <th class="alert alert-info" colspan="4" style="text-align:center;">Data Paket C</th>
                            </tr>
                        </thead>
                        <thead>
                            <tr>
                                <th style="text-align:center;">Nama Lembaga</th>
                                <th style="text-align:center;">Tahun Masuk</th>
                                <th style="text-align:center;">Tahun Lulus  </th>
                                <th style="text-align:center;">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (isset($paket_c))
                                <tr>
                                    <td style="text-align:center;">{{$paket_c->name}}</td>
                                    <td style="text-align:center;">{{$paket_c->entry_year}}</td>
                                    <td style="text-align:center;">{{$paket_c->graduation_year}}</td>
                                    <td style="text-align:center;">
                                        <a onclick="editPendidikan('{{$paket_c->id}}')" class="btn btn-sm btn-warning btn-flat">Edit</a>
                                        <a onclick="deletePendidikan('{{$paket_c->id}}')" class="btn btn-sm btn-danger btn-flat">Hapus</a>
                                    </td>
                                </tr>
                            @else
                                <tr>
                                    <td colspan="4" style="text-align:center;">Kosong</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="card card-warning card-outline addPart" style="display:none;">
                <div class="card-header">
                    <h3 class="card-title">Isi Pendidikan Anda (Isi Sesuai Kualifikasi Pendidikan Anda, Tidak Harus Semua)</h3>
                    <button aria-hidden="true" class="close" onclick="closeAddPart()" type="button">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <div id="accordion">
                        <!-- we are adding the .class so bootstrap.js collapse plugin detects it -->
                        @include('applicants.education.add-sd')
                        @include('applicants.education.add-smp')
                        @include('applicants.education.add-smk-sma')
                        @include('applicants.education.add-strata1')
                        @include('applicants.education.add-pasca-sarjana')
                        @include('applicants.education.add-paketc')
                    </div>
                </div>
            </div>
        {{-- @endif --}}
    </div>
</div>
<div class="modal fade" id="edit-pendidikan">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Edit Pendidikan</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
            </div>
            <form id="frmEditPendidikan">
                <div class="modal-body">
                    <span id="form_result_edit"></span>
                    <div class="form-group">
                        <label>Name</label>
                        <input type="text" name="name" class="form-control" placeholder="Cth: Universitas Mulawarman">
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label>Tahun Masuk</label>
                                <input type="number" name="entry_year" class="form-control" placeholder="Cth: 2010">
                            </div>
                            <div class="col-md-6">
                                <label>Tahun Lulus</label>
                                <input type="number" name="graduation_year" class="form-control" placeholder="Cth: 2019">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="type">
                    <input type="hidden" name="id">
                    <input type="submit" value="Simpan" class="btn bg-maroon btn-flat go">
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="delete-pendidikan">
	<div class="modal-dialog">
		<div class="modal-content">
			<form id="form_delete_pendidikan">
				<div class="modal-header">
					<h4 class="modal-title" id="delete-title" name="title">Hapus Data Pendidikan</h4>
					<button type="button" aria-hidden="true" class="close" data-dismiss="modal">
						x
					</button>
				</div>
				<div class="modal-body">
					<span id="form_result_delete"></span>
					<p>Anda Yakin Ingin Menghapus Data Ini?</p>
					<p class="text-warning">
						<small>Tindakan Ini Tidak Bisa Dibatalkan</small>
					</p>
				</div>
				<div class="modal-footer">
					<input type="hidden" id="id" name="id">
					<button class="btn btn-danger" id="btn-delete" type="button">
						Delete
					</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection
@push('js')
<script src="{{asset('frontend/data/education.js')}}"></script>
@endpush
