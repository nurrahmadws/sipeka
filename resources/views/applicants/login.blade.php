<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Sipeka</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('backend/css/fontawesome/css/all.min.css') }}">
    <link rel="stylesheet" href="{{asset('logreg/css/material-design-iconic-font.min.css')}}">
    <link rel="stylesheet" href="{{asset('logreg/css/style.css')}}">
</head>
<body>
<div class="main">
<!-- Sing in  Form -->
<section class="sign-in">
    <div class="container">
        <div class="signin-content">
            <div class="signin-image">
                <figure><img src="{{asset('logreg/images/signin-image.jpg')}}" alt="sing up image"></figure>
                <a href="{{url('applicant/register')}}" class="signup-image-link">Buat Akun Baru</a>
            </div>
            @include('alert')
            <div class="signin-form">
                <h2 class="form-title">Login</h2>
                <form method="POST" class="register-form" id="login-form">
                    @csrf
                    <div class="form-group">
                        <label for="email"><i class="fas fa-envelope"></i></label>
                        <input type="email" name="email" id="email" placeholder="Email Anda"/>
                    </div>
                    <div class="form-group">
                        <label for="password"><i class="fas fa-lock"></i></label>
                        <input type="password" name="password" id="password" placeholder="Password"/>
                    </div>
                    <div class="form-group">
                        <input type="checkbox" name="remember-me" id="remember-me" class="agree-term" />
                        <label for="remember-me" class="label-agree-term"><span><span></span></span>Remember me</label>
                    </div>
                    <div class="form-group form-button">
                        <input type="submit" name="signin" id="signin" class="form-submit" value="Log in"/>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
</div>
<script src="{{asset('logreg/js/jquery.min.js')}}"></script>
<script src="{{asset('logreg/js/main.js')}}"></script>
</body>
</html>
