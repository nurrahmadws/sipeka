<div class="modal fade" id="edit_berkas">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Edit Berkas</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
            </div>
            <form id="frmEditBerkas">
                <div class="modal-body">
                    <span id="form_result_edit"></span>
                    <div class="form-group">
                        <label>Pilih Kategori Berkas</label>
                        <select name="title" class="form-control">
                            <option value="Curriculum Vitae (CV)">Curriculum Vitae (CV)</option>
                            <option value="Portfolio">Portfolio</option>
                            <option value="KTP">KTP</option>
                            <option value="SIM A">SIM A (Jika Ada)</option>
                            <option value="SIM C">SIM C (Jika Ada)</option>
                            <option value="Ijazah Terakhir">Ijazah Terakhir</option>
                            <option value="Sertifikat/Penghargaan">Sertifikat/Penghargaan (Jika Ada)</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Berkas Saat Ini</label>
                        <br>
                        <span id="berkas"></span>
                    </div>
                    <div class="form-group">
                        <label>Upload Berkas</label>
                        <input type="file" name="file" class="form-control">
                        <span class="text-danger">Berkas Harus Berupa Gambar</span>
                    </div>
                    <div class="form-group">
                        <label>Deskripsi Berkas</label>
                        <input type="text" name="description" class="form-control" placeholder="Cth: Berkas Ijazah Terakhir">
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="id">
                    <input type="submit" value="Simpan" class="btn bg-maroon btn-flat go">
                </div>
            </form>
        </div>
    </div>
</div>
