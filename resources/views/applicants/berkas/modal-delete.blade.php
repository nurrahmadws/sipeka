<div class="modal fade" id="delete-berkas">
	<div class="modal-dialog">
		<div class="modal-content">
			<form id="form_delete_berkas">
				<div class="modal-header">
					<h4 class="modal-title" id="delete-title" name="title">Hapus Berkas</h4>
					<button type="button" aria-hidden="true" class="close" data-dismiss="modal">
						x
					</button>
				</div>
				<div class="modal-body">
					<span id="form_result_delete"></span>
					<p>Anda Yakin Ingin Menghapus Berkas Ini?</p>
					<p class="text-warning">
						<small>Tindakan Ini Tidak Bisa Dibatalkan</small>
					</p>
				</div>
				<div class="modal-footer">
					<input type="hidden" id="id" name="id">
					<button class="btn btn-danger" id="btn-delete" type="button">
						Delete
					</button>
				</div>
			</form>
		</div>
	</div>
</div>
