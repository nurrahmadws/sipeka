@extends('applicants.layout')
@section('title')
    Berkas Saya
@endsection
@section('css')
<style>
    .img{
        display: block;
        margin-left: auto;
        margin-right: auto;
        width:200px;
        height:200px;
        object-fit: cover;
    }
</style>
@endsection
@section('content')
<br>
<br>
<div class="float-left">
    <a onclick="ShowAddPart()" class="btn btn-sm bg-maroon btn-flat">Upload Berkas</a>
</div>
<br>
<hr>
<div class="row">
    @foreach ($berkas as $item)
    <div class="col-md-3 mt-3 mb-5">
        <div class="card card-primary card-outline">
            <div class="card-header">
                {{$item->title}}
            </div>
            <div class="card-body">
                <a href="{{asset('berkas/'.$item->file)}}" target="__blank"><img src="{{asset('berkas/'.$item->file)}}" class="img-thumbnail img"></a>
            </div>
            <div class="card-footer">
                <div class="d-flex justify-content-center">
                    <a onclick="editBerkas('{{$item->id}}')" class="btn btn-warning btn-xs" title="edit">
                        <i class="fab fa-chrome"></i>
                    </a>
                    <a onclick="deleteBerkas('{{$item->id}}')" class="btn bg-maroon btn-xs" title="delete">
                        <i class="fas fa-trash"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
    @endforeach
</div>
@include('applicants.berkas.modal-add')
@include('applicants.berkas.modal-edit')
@include('applicants.berkas.modal-delete')
@endsection
@push('js')
<script src="{{asset('frontend/data/berkas.js')}}"></script>
<script>
function editBerkas(id)
{
    $.ajax({
        type: "GET",
        url: "/applicant/berkas/" + id,
        success: function (data) {
            console.log(data);
            $("#edit_berkas").modal('show');
            $("#frmEditBerkas select[name=title]").val(data.detail.title);
            $("#frmEditBerkas input[name=id]").val(data.detail.id);
            $("#frmEditBerkas input[name=description]").val(data.detail.description);
            $('#berkas').html("<img src={{ URL::to('/') }}/berkas/" + data.detail.file + " width='70' class='img-thumbnail' />");
            // $('#berkas').append("<input type='hidden' name='hidden_image' value='"+html.data.logo+"' />");
        },
        error: function(data){
            console.log(data);
        }
    });
}
</script>
@endpush
