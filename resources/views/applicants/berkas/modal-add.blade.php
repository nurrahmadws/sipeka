<div class="modal fade" id="upload_berkas">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Upload Berkas</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
            </div>
            <form id="frmUploadBerkas">
                <div class="modal-body">
                    <span id="form_result"></span>
                    <div class="form-group">
                        <label>Pilih Kategori Berkas Yang Ingin Diupload</label>
                        <select name="title" class="form-control">
                            <option value="Curriculum Vitae (CV)"
                            @if (isset($cv))
                                disabled
                            @else

                            @endif>Curriculum Vitae (CV)</option>
                            <option value="Portfolio"
                            @if (isset($portfolio))
                                disabled
                            @else

                            @endif>Portfolio</option>
                            <option value="KTP"
                            @if (isset($ktp))
                                disabled
                            @else

                            @endif>KTP</option>
                            <option value="SIM A"
                            @if (isset($sima))
                                disabled
                            @else

                            @endif>SIM A (Jika Ada)</option>
                            <option value="SIM C"
                            @if (isset($simc))
                                disabled
                            @else

                            @endif>SIM C (Jika Ada)</option>
                            <option value="Ijazah Terakhir"
                            @if (isset($ijazah))
                                disabled
                            @else

                            @endif>Ijazah Terakhir</option>
                            <option value="Sertifikat/Penghargaan"
                            @if (isset($sertifikat))
                                disabled
                            @else

                            @endif>Sertifikat/Penghargaan (Jika Ada)</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Upload Berkas</label>
                        <input type="file" name="file" class="form-control">
                        <span class="text-danger">Berkas Harus Berupa Gambar</span>
                    </div>
                    <div class="form-group">
                        <label>Deskripsi Berkas</label>
                        <input type="text" name="description" class="form-control" placeholder="Cth: Berkas Ijazah Terakhir">
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="submit" value="Simpan" class="btn bg-navy btn-flat go">
                </div>
            </form>
        </div>
    </div>
</div>
