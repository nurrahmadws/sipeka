<div class="card card-danger card-outline edtbio" style="display:none;">
    <div class="card-header">
        <h3 class="card-title">Edit Biodata Saya</h3>
        <button aria-hidden="true" class="close" onclick="closeEditData()" type="button">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <form id="frmEdtBiodata">
        @csrf
        <div class="card-body">
            @include('alert')
            <span id="form_result_edit"></span>
            <div class="form-group">
                <label>Nama Lengkap</label>
                <input type="text" class="form-control" name="name">
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    <label>Tempat Lahir</label>
                    <input type="text" class="form-control" name="date_place" placeholder="Cth: Samarinda">
                </div>
                <div class="col-md-6">
                    <label>Tanggal Lahir</label>
                    <div class="input-group date">
                        <input type="date" name="date_birth" class="form-control">
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    <label>Jenis Kelamin</label> <br>
                    <label class="radio-inline"><input type="radio" name="gender" value="Laki - Laki"
                        {{$biodata->gender == 'Laki - Laki' ? 'checked' : ''}}>Laki - Laki</label>
                    <label class="radio-inline"><input type="radio" name="gender" value="Perempuan"
                        {{$biodata->gender == 'Perempuan' ? 'checked' : ''}}>Perempuan</label>
                </div>
                <div class="col-md-6">
                    <label >Agama</label> <br>
                    <label class="radio-inline"><input type="radio" name="religion" value="Islam"
                        {{$biodata->religion == 'Islam' ? 'checked' : ''}}>Islam</label>
                    <label class="radio-inline"><input type="radio" name="religion" value="Protestan"
                        {{$biodata->religion == 'Protestan' ? 'checked' : ''}}>Protestan</label>
                    <label class="radio-inline"><input type="radio" name="religion" value="Katholik"
                        {{$biodata->religion == 'Katholik' ? 'checked' : ''}}>Katholik</label>
                    <label class="radio-inline"><input type="radio" name="religion" value="Hindu"
                        {{$biodata->religion == 'Hindu' ? 'checked' : ''}}>Hindu</label>
                    <label class="radio-inline"><input type="radio" name="religion" value="Buddha"
                        {{$biodata->religion == 'Buddha' ? 'checked' : ''}}>Buddha</label>
                    <label class="radio-inline"><input type="radio" name="religion" value="Konghucu"
                        {{$biodata->religion == 'Konghucu' ? 'checked' : ''}}>Konghucu</label>
                    <label class="radio-inline"><input type="radio" name="religion" value="Lain - Lain"
                        {{$biodata->religion == 'Lain - Lain' ? 'checked' : ''}}>Lain - Lain</label>
                </div>
            </div>
            <div class="form-group">
                <label>Pendidikan Terakhir</label> <br>
                <label class="radio-inline"><input type="radio" name="last_education" value="SD"
                    {{$biodata->last_education == 'SD' ? 'checked' : ''}}>SD</label>
                <label class="radio-inline"><input type="radio" name="last_education" value="SMP"
                    {{$biodata->last_education == 'SMP' ? 'checked' : ''}}>SMP</label>
                <label class="radio-inline"><input type="radio" name="last_education" value="SMK"
                    {{$biodata->last_education == 'SMK' ? 'checked' : ''}}>SMK</label>
                <label class="radio-inline"><input type="radio" name="last_education" value="SMA"
                    {{$biodata->last_education == 'SMA' ? 'checked' : ''}}>SMA</label>
                <label class="radio-inline"><input type="radio" name="last_education" value="Strata 1"
                    {{$biodata->last_education == 'Strata 1' ? 'checked' : ''}}>Strata 1</label>
                <label class="radio-inline"><input type="radio" name="last_education" value="Pasca Sarjana"
                    {{$biodata->last_education == 'Pasca Sarjana' ? 'checked' : ''}}>Pasca Sarjana</label>
                <label class="radio-inline"><input type="radio" name="last_education" value="Lain - Lain"
                    {{$biodata->last_education == 'Lain - Lain' ? 'checked' : ''}}>Lain - Lain</label>
            </div>
            <div class="form-group">
                <label>Alamat Berdasarkan KTP</label>
                <textarea name="address_based_id_card" class="form-control" placeholder="Cth: Jl. Poros Desa Liang Ulu Kecamatan Pondok Kecil, Kabupaten Ini"></textarea>
            </div>
            <div class="form-group">
                <label>Alamat Sekarang</label>
                <textarea name="current_address" class="form-control" placeholder="Cth: Jl. H. Gofur Komplek Villa Cilame Blok C, Bandung Barat"></textarea>
            </div>
            <div class="form-group">
                <label>No Handphone/WA</label>
                <input type="number" name="phone" placeholder="Cth: 08129299922" class="form-control">
            </div>
            <div class="form-group">
                <label>Email</label>
                <input type="email" name="email" value="{{$user->email}}" class="form-control" disabled>
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    <label>Identitas</label> <br>
                    <label class="radio-inline"><input type="radio" name="identity" value="KTP"
                        {{$biodata->identity == 'KTP' ? 'checked' : ''}}>KTP</label>
                    <label class="radio-inline"><input type="radio" name="identity" value="Kartu Keluarga"
                        {{$biodata->identity == 'Kartu Keluarga' ? 'checked' : ''}}>Kartu Keluarga</label>
                    <label class="radio-inline"><input type="radio" name="identity" value="SIM C"
                        {{$biodata->identity == 'SIM C' ? 'checked' : ''}}>SIM C</label>
                    <label class="radio-inline"><input type="radio" name="identity" value="SIM B"
                        {{$biodata->identity == 'SIM B' ? 'checked' : ''}}>SIM B</label>
                    <label class="radio-inline"><input type="radio" name="identity" value="SIM A"
                        {{$biodata->identity == 'SIM A' ? 'checked' : ''}}>SIM A</label>
                    <label class="radio-inline"><input type="radio" name="identity" value="Lain - Lain"
                        {{$biodata->identity == 'Lain - Lain' ? 'checked' : ''}}>Lain - Lain</label>
                </div>
                <div class="col-md-6">
                    <label>Masukkan Nomor Identitas</label>
                    <input type="text" name="id_identity" class="form-control" placeholder="Cth: 640802099290">
                </div>
            </div>
            <div class="form-group">
                <label>Status Pernikahan</label> <br>
                <label class="radio-inline"><input type="radio" name="marital_status" value="Belum Menikah"
                    {{$biodata->marital_status == 'Belum Menikah' ? 'checked' : ''}}>Belum Menikah</label>
                <label class="radio-inline"><input type="radio" name="marital_status" value="Sudah Menikah"
                    {{$biodata->marital_status == 'Sudah Menikah' ? 'checked' : ''}}>Sudah Menikah</label>
                <label class="radio-inline"><input type="radio" name="marital_status" value="Janda"
                    {{$biodata->marital_status == 'Janda' ? 'checked' : ''}}>Janda</label>
                <label class="radio-inline"><input type="radio" name="marital_status" value="Duda"
                    {{$biodata->marital_status == 'Duda' ? 'checked' : ''}}>Duda</label>
            </div>
            <div class="form-group">
                <label>Photo Saat Ini</label> <br>
                <img src="{{asset('image/'.$biodata->photo)}}" class="img-thumbnail">
            </div>
            <div class="form-group">
                <label>Photo</label>
                <input type="file" name="photo" class="form-control">
            </div>
        </div>
        <div class="card-footer">
            <input type="hidden" name="id">
            <input type="submit" class="btn btn-primary btn-flat btn-block go" value="Simpan Data">
        </div>
    </form>
</div>
