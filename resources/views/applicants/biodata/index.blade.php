@extends('applicants.layout')
@section('title')
Biodata
@endsection
@section('css')
@endsection
@section('content')
<div class="row">
    <div class="col-md-10 offset-1 mt-5 mb-5">
        @if($errors->any())
            <div class="alert alert-primary" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <b>Message : </b>{{$errors->first()}}
            </div>
        @endif

        @if ($biodata == null)
        <div class="alert alert-danger msg">
            <h5><i class="icon fas fa-info"></i> Data Tidak Ada</h5>
            Anda Belum Mengisi Biodata Anda, Harap Isi Biodata Anda Terlebih Dahulu, Klil <a onclick="CreateBiodata()"><u><i>Disini</i></u></a>
        </div>
        @include('applicants.biodata.add-biodata')
        @else
        <div class="card card-primary card-outline myBio">
            <div class="card-header">
                <h3 class="card-title">Biodata Saya</h3>
            </div>
            <div class="card-body">
                <table class="table">
                    <tr>
                        <th>Nama Lengkap</th>
                        <td>:</td>
                        <td>{{$biodata->name}}</td>
                    </tr>
                    <tr>
                        <th>Tempat, Tanggal Lahir</th>
                        <td>:</td>
                        <td>{{$biodata->date_place}}, {{\Carbon\Carbon::parse($biodata->date_birth)->format('d F Y')}}</td>
                    </tr>
                    <tr>
                        <th>Jenis Kelamin</th>
                        <td>:</td>
                        <td>{{$biodata->gender}}</td>
                    </tr>
                    <tr>
                        <th>Agama</th>
                        <td>:</td>
                        <td>{{$biodata->religion}}</td>
                    </tr>
                    <tr>
                        <th>Pendidikan Terakhir</th>
                        <td>:</td>
                        <td>{{$biodata->last_education}}</td>
                    </tr>
                    <tr>
                        <th>Alamat (Berdasarkan KTP)</th>
                        <td>:</td>
                        <td>{{$biodata->address_based_id_card}}</td>
                    </tr>
                    <tr>
                        <th>Alamat Sekarang</th>
                        <td>:</td>
                        <td>{{$biodata->current_address}}</td>
                    </tr>
                    <tr>
                        <th>No Handphone</th>
                        <td>:</td>
                        <td>{{$biodata->phone}}</td>
                    </tr>
                    <tr>
                        <th>Email</th>
                        <td>:</td>
                        <td>{{$user->email}}</td>
                    </tr>
                    <tr>
                        <th>Kartu Identitas</th>
                        <td>:</td>
                        <td>{{$biodata->identity}}</td>
                    </tr>
                    <tr>
                        <th>Nomor Kartu Identitas</th>
                        <td>:</td>
                        <td>{{$biodata->id_identity}}</td>
                    </tr>
                    <tr>
                        <th>Status Pernikahan</th>
                        <td>:</td>
                        <td>{{$biodata->marital_status}}</td>
                    </tr>
                    <tr>
                        <th>Photo</th>
                        <td>:</td>
                        <td><img src="{{asset('image/'.$biodata->photo)}}" class="img-thumbnail"></td>
                    </tr>
                </table>
            </div>
            <div class="card-footer">
                <a onclick="editBiodata('{{ $biodata->id }}')" class="btn bg-maroon btn-flat">Edit</a>
            </div>
        </div>
        @include('applicants.biodata.edit-biodata')
        @endif
    </div>
</div>
@endsection
@push('js')
<script src="{{asset('frontend/data/biodata.js')}}"></script>
@endpush
