<div class="card card-success card-outline bio" style="display:none;">
    <div class="card-header">
        <h3 class="card-title">Buat Biodata Baru</h3>
        <button aria-hidden="true" class="close" onclick="closeCreateData()" type="button">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <form id="frmAddBiodata">
        @csrf
        <div class="card-body">
            @include('alert')
            <span id="form_result"></span>
            <div class="form-group">
                <label>Nama Lengkap</label>
                <input type="text" class="form-control" name="name" value="{{$user->name}}">
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    <label>Tempat Lahir</label>
                    <input type="text" class="form-control" name="date_place" placeholder="Cth: Samarinda">
                </div>
                <div class="col-md-6">
                    <label>Tanggal Lahir</label>
                    <div class="input-group date">
                        <input type="date" name="date_birth" class="form-control">
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    <label>Jenis Kelamin</label> <br>
                    <label class="radio-inline"><input type="radio" name="gender" value="Laki - Laki">Laki - Laki</label>
                    <label class="radio-inline"><input type="radio" name="gender" value="Perempuan">Perempuan</label>
                </div>
                <div class="col-md-6">
                    <label >Agama</label> <br>
                    <label class="radio-inline"><input type="radio" name="religion" value="Islam">Islam</label>
                    <label class="radio-inline"><input type="radio" name="religion" value="Protestan">Protestan</label>
                    <label class="radio-inline"><input type="radio" name="religion" value="Katholik">Katholik</label>
                    <label class="radio-inline"><input type="radio" name="religion" value="Hindu">Hindu</label>
                    <label class="radio-inline"><input type="radio" name="religion" value="Buddha">Buddha</label>
                    <label class="radio-inline"><input type="radio" name="religion" value="Konghucu">Konghucu</label>
                    <label class="radio-inline"><input type="radio" name="religion" value="Lain - Lain">Lain - Lain</label>
                </div>
            </div>
            <div class="form-group">
                <label>Pendidikan Terakhir</label> <br>
                <label class="radio-inline"><input type="radio" name="last_education" value="SD">SD</label>
                <label class="radio-inline"><input type="radio" name="last_education" value="SMP">SMP</label>
                <label class="radio-inline"><input type="radio" name="last_education" value="SMK">SMK</label>
                <label class="radio-inline"><input type="radio" name="last_education" value="SMA">SMA</label>
                <label class="radio-inline"><input type="radio" name="last_education" value="Strata 1">Strata 1</label>
                <label class="radio-inline"><input type="radio" name="last_education" value="Pasca Sarjana">Pasca Sarjana</label>
                <label class="radio-inline"><input type="radio" name="last_education" value="Lain - Lain">Lain - Lain</label>
            </div>
            <div class="form-group">
                <label>Alamat Berdasarkan KTP</label>
                <textarea name="address_based_id_card" class="form-control" placeholder="Cth: Jl. Poros Desa Liang Ulu Kecamatan Pondok Kecil, Kabupaten Ini"></textarea>
            </div>
            <div class="form-group">
                <label>Alamat Sekarang</label>
                <textarea name="current_address" class="form-control" placeholder="Cth: Jl. H. Gofur Komplek Villa Cilame Blok C, Bandung Barat"></textarea>
            </div>
            <div class="form-group">
                <label>No Handphone/WA</label>
                <input type="number" name="phone" placeholder="Cth: 08129299922" class="form-control">
            </div>
            <div class="form-group">
                <label>Email</label>
                <input type="email" name="email" value="{{$user->email}}" class="form-control" disabled>
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    <label>Identitas</label> <br>
                    <label class="radio-inline"><input type="radio" name="identity" value="KTP">KTP</label>
                    <label class="radio-inline"><input type="radio" name="identity" value="Kartu Keluarga">Kartu Keluarga</label>
                    <label class="radio-inline"><input type="radio" name="identity" value="SIM C">SIM C</label>
                    <label class="radio-inline"><input type="radio" name="identity" value="SIM B">SIM B</label>
                    <label class="radio-inline"><input type="radio" name="identity" value="SIM A">SIM A</label>
                    <label class="radio-inline"><input type="radio" name="identity" value="Lain - Lain">Lain - Lain</label>
                </div>
                <div class="col-md-6">
                    <label>Masukkan Nomor Identitas</label>
                    <input type="text" name="id_identity" class="form-control" placeholder="Cth: 640802099290">
                </div>
            </div>
            <div class="form-group">
                <label>Status Pernikahan</label> <br>
                <label class="radio-inline"><input type="radio" name="marital_status" value="Belum Menikah">Belum Menikah</label>
                <label class="radio-inline"><input type="radio" name="marital_status" value="Sudah Menikah">Sudah Menikah</label>
                <label class="radio-inline"><input type="radio" name="marital_status" value="Janda">Janda</label>
                <label class="radio-inline"><input type="radio" name="marital_status" value="Duda">Duda</label>
            </div>
            <div class="form-group">
                <label>Photo</label>
                <input type="file" name="photo" class="form-control">
            </div>
        </div>
        <div class="card-footer">
            <input type="submit" class="btn btn-primary btn-flat btn-block go" value="Simpan Data">
        </div>
    </form>
</div>
