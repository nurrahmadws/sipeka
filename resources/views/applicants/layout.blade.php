<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Sipeka | @yield('title')</title>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend/css/fontawesome/css/all.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend/css/adminlte.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend/css/OverlayScrollbars.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend/css/custom.css') }}">
    @yield('css')
</head>
<body class="hold-transition sidebar-mini layout-fixed">

    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
            </li>
            <li class="nav-item d-none d-sm-inline-block">
                <a href="{{ url('/applicant/home') }}" class="nav-link">Home</a>
            </li>
        </ul>
        <!-- Right navbar links -->
        <ul class="navbar-nav ml-auto">
            <!-- Notifications Dropdown Menu -->
            <li class="nav-item dropdown">
                <a class="nav-link" data-toggle="dropdown" href="#">
                    <i class="far fa-bell"></i>
                    <span class="badge badge-warning navbar-badge">15</span>
                </a>
                <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                    <span class="dropdown-item dropdown-header">{{session('user_front')}}</span>
                    <div class="dropdown-divider"></div>
                    <a id="editMyProfile" onclick="EditMyProfile('{{session('userId')}}')" class="dropdown-item">
                        <i class="fas fa-terminal mr-2"></i> Edit My Profile
                    </a>
                    <div class="dropdown-divider"></div>
                    <a id="editMyPassword" onclick="EditMyPassword('{{ session('userId') }}')" class="dropdown-item">
                        <i class="fas fa-leaf mr-2"></i> Edit My Password
                    </a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="/applicant/logout"
                    onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
                    <i class="fas fa-sign-out-alt"></i> {{ __('Logout') }}
                </a>
                <form id="logout-form" action="/applicant/logout" method="POST" style="display: none;">
                    @csrf
                </form>
            </div>
        </li>
    </ul>
</nav>

<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="info">
                <a href="#" class="d-block">{{ session('user_front')}}</a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                    with font-awesome or any other icon font library -->
                    <li class="nav-item has-treeview">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-cubes"></i>
                            <p>
                                Data Pribadi
                                <i class="fas fa-angle-left right"></i>
                                <span class="badge badge-info right">3</span>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{ url('applicant/biodata') }}" title="Biodata" class="nav-link">
                                    <i class="fas fa-book nav-icon"></i>
                                    <p>Biodata</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ url('applicant/education') }}" title="Pendidikan" class="nav-link">
                                    <i class="fas fa-graduation-cap nav-icon"></i>
                                    <p>Pendidikan</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ url('applicant/berkas') }}" title="Berkas" class="nav-link">
                                    <i class="fas fa-server nav-icon"></i>
                                    <p>Berkas</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item">
                        <a href="{{url('applicant/lamaran')}}" class="nav-link">
                            <i class="nav-icon far fa-list-alt"></i>
                            <p>
                                Apply Lamaran
                            </p>
                        </a>
                    </li>
                </ul>
            </nav>
            <!-- /.sidebar-menu -->
        </div>
        <!-- /.sidebar -->
    </aside>

    <div class="content-wrapper">
        <section class="content">
            @yield('content')
        </section>
    </div>
    <div class="modal fade" id="edit-my-profile">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Edit My Profile</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="edit_my_profile">
                    <div class="modal-body">
                        <span id="form_result_edit"></span>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fas fa-user"></i></span>
                            </div>
                            <input type="text" class="form-control" name="name" id="name" placeholder="Nama Lengkap" required>
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                            </div>
                            <input type="email" name="email" id="email" class="form-control" placeholder="Email" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="id" id="id">
                        <input type="submit" value="Simpan" class="btn btn-primary go">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="edit-my-password">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Edit My Password</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="edit_my_password">
                    <div class="modal-body">
                        <span id="form_result_password"></span>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fas fa-lock"></i></span>
                            </div>
                            <input type="password" name="old_password" id="old_password" placeholder="Password Lama" class="form-control" required>
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fas fa-lock"></i></span>
                            </div>
                            <input type="password" name="password" id="password" class="form-control" placeholder="Password Baru" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="id" id="id">
                        <input type="submit" value="Simpan" class="btn btn-primary go">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script src="{{ asset('frontend/js/jquery.min.js') }}"></script>
    <script src="{{ asset('frontend/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('frontend/js/adminlte.min.js') }}"></script>
    <script src="{{ asset('frontend/js/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('frontend/js/jquery.overlayScrollbars.min.js') }}"></script>
    @stack('js')
    <script>
        function EditMyProfile(id){
            $.ajax({
                type: "GET",
                url: "/applicant/show-user/" + id,
                success: function (data) {
                    console.log(data);
                    $("#edit-my-profile").modal('show');
                    $("#edit_my_profile input[name=name]").val(data.detail.name);
                    $("#edit_my_profile input[name=email]").val(data.detail.email);
                    $("#edit_my_profile input[name=id]").val(data.detail.id);
                },
                error: function(data){
                    console.log(data);
                }
            });
        }

        function EditMyPassword(id){
            $.ajax({
                type: "GET",
                url: "/applicant/show-user/" + id,
                success: function (data) {
                    console.log(data);
                    $("#edit-my-password").modal('show');
                    $("#edit_my_password input[name=id]").val(data.detail.id);
                },
                error: function(data){
                    console.log(data);
                }
            });
        }

        $(document).ready(function(){
            $("#edit_my_profile").on('submit', function(e){
                e.preventDefault();
                $.ajaxSetup({
                    headers:{
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: "/applicant/update-user/" + $("#edit_my_profile input[name=id]").val(),
                    data: new FormData(this),
                    contentType: false,
                    cache:false,
                    processData: false,
                    dataType: "json",
                    beforeSend: function(){
                        $('.go').addClass('btn-progress');
                    },
                    success: function (response) {

                        var html = '';
                        if(response.errors){
                            html = '<div class="alert alert-danger">';
                                for(var count = 0; count < response.errors.length; count++)
                                {
                                    html += '<p>' + response.errors[count] + "</p>";
                                }
                                html += '</div>';
                                $('.go').removeClass('btn-progress');
                            }
                            if (response.success) {
                                html = '<div class="alert alert-success"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + response.success + '</div>';
                                $('#edit_my_profile')[0].reset();
                                setTimeout(function(){
                                    location.reload();
                                }, 1000);
                            }
                            $('#form_result_edit').html(html);
                        }
                    });
                });
            $("#edit_my_password").on('submit', function(e){
                e.preventDefault();
                $.ajaxSetup({
                    headers:{
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: "/applicant/update-password/" + $("#edit_my_password input[name=id]").val(),
                    data: new FormData(this),
                    contentType: false,
                    cache:false,
                    processData: false,
                    dataType: "json",
                    beforeSend: function(){
                        $('.go').addClass('btn-progress');
                    },
                    success: function (response) {

                        var html = '';
                        if(response.errors){
                            html = '<div class="alert alert-danger">';
                                for(var count = 0; count < response.errors.length; count++)
                                {
                                    html += '<p>' + response.errors[count] + "</p>";
                                }
                                html += '</div>';
                                $('.go').removeClass('btn-progress');
                            }
                            if (response.success) {
                                html = '<div class="alert alert-success"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + response.success + '</div>';
                                $('#edit_my_password')[0].reset();
                                setTimeout(function(){
                                    location.reload();
                                }, 3000);
                            }
                            $('#form_result_password').html(html);
                        }
                    });
                });
        });
        </script>
    </body>
    </html>
