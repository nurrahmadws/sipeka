<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Sipeka</title>
    <link rel="stylesheet" href="{{asset('logreg/css/material-design-iconic-font.min.css')}}">
    <link rel="stylesheet" href="{{asset('logreg/css/style.css')}}">
</head>
<body>
<div class="main">
<section class="sign-in">
    <div class="container">
        <h2 style="margin-left:70px;">Selamat Datang Di Sistem Pendaftaran Karyawan</h2>
        <div class="signin-content">
            <div class="signin-image">
                <figure><img src="{{asset('logreg/images/signin-image.jpg')}}" alt="sing up image"></figure>
                <a href="{{url('applicant/login')}}" class="signup-image-link">Login Sebagai Pelamar</a>
            </div>

            <div class="signin-form">
                <figure><img src="{{asset('logreg/images/signup-image.jpg')}}" alt="sing up image"></figure>
                <br>
                <br>
                <a href="{{url('hrd/login')}}" class="signup-image-link">Login HRD</a>
            </div>
        </div>
    </div>
</section>
</div>
<script src="{{asset('logreg/js/jquery.min.js')}}"></script>
<script src="{{asset('logreg/js/main.js')}}"></script>
</body>
</html>
