<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::prefix('applicant')->group(function(){
    Route::get('login', 'Frontend\AuthController@formLogin');
    Route::get('register', 'Frontend\AuthController@formRegister');
    Route::post('register-process','Frontend\AuthController@register');
    Route::post('login','Frontend\AuthController@login');
    Route::post('logout','Frontend\AuthController@logout');
    Route::get('show-user/{id}','Frontend\AuthController@show');
    Route::post('update-user/{id}','Frontend\AuthController@editMyProfile');
    Route::post('update-password/{id}','Frontend\AuthController@editMyPassword');
    Route::get('home', 'Frontend\HomeController@index');
    Route::get('biodata', 'Frontend\BiodataController@index');
    Route::post('biodata', 'Frontend\BiodataController@store');
    Route::get('biodata/{id}', 'Frontend\BiodataController@show');
    Route::post('biodataa/{id}', 'Frontend\BiodataController@update');
    Route::get('education', 'Frontend\EducationController@index');
    Route::post('education', 'Frontend\EducationController@store');
    Route::get('education/{id}', 'Frontend\EducationController@show');
    Route::post('educationn/{id}', 'Frontend\EducationController@update');
    Route::delete('educationn/{id}', 'Frontend\EducationController@destroy');
    Route::get('berkas', 'Frontend\BerkasController@index');
    Route::post('berkas', 'Frontend\BerkasController@store');
    Route::get('berkas/{id}', 'Frontend\BerkasController@show');
    Route::post('berkass/{id}', 'Frontend\BerkasController@update');
    Route::delete('berkass/{id}', 'Frontend\BerkasController@destroy');
    Route::get('lamaran', 'Frontend\LamaranController@index');
    Route::post('lamaran', 'Frontend\LamaranController@store');
    Route::get('lamaran/{id}', 'Frontend\LamaranController@detail');
    Route::get('lamaran/{id}/edit', 'Frontend\LamaranController@edit');
    Route::post('lamaran/{id}/update', 'Frontend\LamaranController@update');
});

Route::prefix('hrd')->group(function(){
    Route::get('login', 'Backend\AuthController@formLogin');
    Route::post('login', 'Backend\AuthController@login');
    Route::post('logout', 'Backend\AuthController@logout');
    Route::get('show-user/{id}','Backend\AuthController@show');
    Route::post('update-user/{id}','Backend\AuthController@editMyProfile');
    Route::post('update-password/{id}','Backend\AuthController@editMyPassword');
    Route::get('home', 'Backend\HomeController@index');
    Route::get('job-category', 'Backend\JobCategoryController@index');
    Route::post('job-category', 'Backend\JobCategoryController@store');
    Route::get('job-category/{id}', 'Backend\JobCategoryController@show');
    Route::post('job-categoryy/{id}', 'Backend\JobCategoryController@update');
    Route::delete('category-job/{id}', 'Backend\JobCategoryController@destroy');
    Route::get('proposed', 'Backend\ProposedController@index');
    Route::get('proposed/{id}/detail', 'Backend\ProposedController@detail');
    Route::post('update-proposed/{id}', 'Backend\ProposedController@update');
});
